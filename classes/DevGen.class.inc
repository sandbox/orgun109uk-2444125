<?php
/**
 * @file
 * Provides the main DevGen class.
 */

/**
 * Class DevGen.
 */
class DevGen {

  /**
   * The name of the devgen.
   *
   * @var string
   */
  private $name;

  /**
   * The devgen config object.
   *
   * @var array
   */
  private $config = array(
    'title' => NULL,
    'description' => NULL,
    'createdOn' => NULL,
    'createdBy' => NULL,
    'modifiedOn' => NULL,
    'modifiedBy' => NULL,
    'settings' => array(),
    'properties' => array(),
    'tasks' => array(),
  );

  /**
   * The current run ID.
   *
   * @var int
   */
  protected $runId = FALSE;

  /**
   * The output object.
   *
   * @var DevGenOutput
   */
  protected $output;

  /**
   * Create a new DevGen container object.
   *
   * @param string $name
   *   The name of the devgen.
   * @param array $config
   *   (optional = NULL) A devgen config array to import, leave as NULL to
   *   create an empty devgen.
   */
  public function __construct($name, array $config = NULL) {
    $this->name = $name;

    if (!empty($config)) {
      $this->import($config);
    }

    module_invoke_all('devgen_construct', $this);
  }

  /**
   * {@inheritdoc}
   */
  final public function __sleep() {
    $this->config = $this->export();
    return array('name', 'config');
  }

  /**
   * {@inheritdoc}
   */
  final public function __wakeup() {
    $this->import($this->config);
    module_invoke_all('devgen_construct', $this);
  }

  /**
   * {@inheritdoc}
   */
  final public function __toString() {
    return json_encode($this->export(), JSON_PRETTY_PRINT);
  }

  /**
   * {@inheritdoc}
   */
  public function __call($method, $arguments) {
    if (isset($this->$method) && is_callable($this->$method)) {
      return call_user_func_array(Closure::bind($this->$method, $this, get_called_class()), $arguments);
    }
  }

  /**
   * Get the current run ID.
   *
   * @return int|bool
   *   Returns the run ID, or FALSE.
   */
  public function getRunId() {
    return $this->runId;
  }

  /**
   * Get the name of this devgen.
   *
   * @return string
   *   The devgens name.
   */
  final public function getName() {
    return $this->name;
  }

  /**
   * Get the title of the devgen container.
   *
   * @return string
   *   Returns the title.
   */
  final public function getTitle() {
    return isset($this->config['title']) ? $this->config['title'] : NULL;
  }

  /**
   * Set the devgens title.
   *
   * @param string $value
   *   The title to apply to the devgen.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function setTitle($value) {
    $this->config['title'] = $value;
    return $this;
  }

  /**
   * Get the description of the devgen container.
   *
   * @return string
   *   Returns the description.
   */
  final public function getDescription() {
    return isset($this->config['description']) ? $this->config['description'] : NULL;
  }

  /**
   * Set the devgens description.
   *
   * @param string $value
   *   The description to apply to the devgen.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function setDescription($value) {
    $this->config['description'] = $value;
    return $this;
  }

  /**
   * Get the UNIX timestamp of when this devgen was created.
   *
   * @return int|null
   *   The created unix timestamp.
   */
  final public function createdOn() {
    return isset($this->config['createdOn']) ? $this->config['createdOn'] : NULL;
  }

  /**
   * Get the name of who created this devgen.
   *
   * @return string|null
   *   The created by user name.
   */
  final public function createdBy() {
    return isset($this->config['createdBy']) ? $this->config['createdBy'] : NULL;
  }

  /**
   * Get the UNIX timestamp of when this devgen was last modified.
   *
   * @return int|null
   *   The modified unix timestamp.
   */
  final public function modifiedOn() {
    return isset($this->config['modifiedOn']) ? $this->config['modifiedOn'] : NULL;
  }

  /**
   * Get the name of who last modified this devgen.
   *
   * @return string|null
   *   The modified by user name.
   */
  final public function modifiedBy() {
    return isset($this->config['modifiedBy']) ? $this->config['modifiedBy'] : NULL;
  }

  /**
   * Get the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $default
   *   (optional = NULL) The default value to return if the setting does'nt
   *   exist.
   *
   * @return mixed
   *   Returns the setting value or the $default.
   */
  final public function getSetting($setting, $default = NULL) {
    return $this->hasSetting($setting) ? $this->config['settings'][$setting] : $default;
  }

  /**
   * Set the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $value
   *   The value to apply to the setting.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function setSetting($setting, $value) {
    $this->config['settings'][$setting] = $value;
    return $this;
  }

  /**
   * Determine if a setting has been supplied.
   *
   * @param string $setting
   *   The setting name.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function hasSetting($setting) {
    return isset($this->config['settings'][$setting]);
  }

  /**
   * Returns an array of all the current properties names.
   *
   * @return array
   *   An array of property names.
   */
  final public function getPropertyNames() {
    return !empty($this->config['properties']) && is_array($this->config['properties']) ?
      array_keys($this->config['properties']) :
      array();
  }

  /**
   * Add a property object.
   *
   * @param DevGenProperty $property
   *   The property object to add.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function addProperty(DevGenProperty $property) {
    $this->config['properties'][$property->getName()] = $property;
    return $this;
  }

  /**
   * Determines if a property name exists.
   *
   * @param string $name
   *   The name of the property to look for.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function hasProperty($name) {
    return isset($this->config['properties'][$name]);
  }

  /**
   * Get the property object.
   *
   * @param string $name
   *   The name of the property.
   *
   * @return DevGenProperty
   *   The property object, otherwise NULL.
   */
  final public function getProperty($name) {
    return $this->hasProperty($name) ? $this->config['properties'][$name] : NULL;
  }

  /**
   * Delete the specified property.
   *
   * @param string $name
   *   The name of the property.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function delProperty($name) {
    unset($this->config['properties'][$name]);
    return $this;
  }

  /**
   * Generate an array of tokens from the properties and tasks available.
   *
   * @param bool $parse
   *   (optional = FALSE) If TRUE the values of the tokens will be the property
   *   and task property values.
   *
   * @return array
   *   Returns the tokens.
   */
  final public function propertyTokens($parse = FALSE) {
    $tokens = array();
    foreach ($this->getPropertyNames() as $name) {
      $tokens['@{' . $name . '}'] = $parse ? $this->getProperty($name)->value() : $name;
    }

    if (!empty($this->config['tasks']) && is_array($this->config['tasks'])) {
      foreach ($this->config['tasks'] as $task) {
        /* @var DevGenTask $task */
        if ($task instanceof DevGenAction === FALSE) {
          continue;
        }

        $tokens = array_merge($tokens, $task->getTokens($parse));
      }
    }

    return $tokens;
  }

  /**
   * Parse tokens on the given value.
   *
   * @param string $value
   *   The value to parse.
   *
   * @return string
   *   The parsed value.
   */
  final public function parseTokens($value) {
    $tokens = $this->propertyTokens(TRUE);
    if (isset($tokens[$value])) {
      return $tokens[$value];
    }

    $tkns = array();
    foreach ($tokens as $token => $val) {
      if (is_object($val) || is_array($val)) {
        continue;
      }

      $tkns[$token] = $val;
    }

    return strtr(strtr($value, $tkns), $tkns);
  }

  /**
   * Returns an array of all the current tasks names.
   *
   * @return array
   *   An array of task names.
   */
  final public function getTaskOptions($type = NULL, $subtype = NULL, $inc_empty = TRUE) {
    $options = array();
    if ($inc_empty) {
      $options[NULL] = is_string($inc_empty) ? $inc_empty : t('- select a task -');
    }

    if (!empty($this->config['tasks']) && is_array($this->config['tasks'])) {
      foreach ($this->config['tasks'] as $name => $task) {
        if ($type && (
          ($type === 'action' && $task instanceof DevGenAction === FALSE) ||
          ($type === 'condition' && $task instanceof DevGenCondition === FALSE)
        )) {
          continue;
        }

        if ($subtype && in_array(devgen_task_to_type(get_class($task)), (array) $subtype) === FALSE) {
          continue;
        }

        $task_name = is_array($task) ? $task['name'] : devgen_task_to_type(get_class($task));
        $options[$name] = $subtype ? $name : '[' . devgen_task_title($task_name) . "] - {$name}";
      }
    }

    return $options;
  }

  /**
   * Returns an array of all the current tasks names.
   *
   * @return array
   *   An array of task names.
   */
  final public function getTaskNames() {
    return !empty($this->config['tasks']) && is_array($this->config['tasks']) ?
      array_keys($this->config['tasks']) :
      array();
  }

  /**
   * Adds a created devgen task.
   *
   * @param DevGenTask $task
   *   The task to add.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function add(DevGenTask $task) {
    $this->config['tasks'][$task->getName()] = $task;

    $entry_point = $this->getSetting('entry_point');
    if (empty($entry_point)) {
      $this->setSetting('entry_point', $task->getName());
    }

    return $this;
  }

  /**
   * Determines if a given task name is already available.
   *
   * @param string $name
   *   The task name to check.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function has($name) {
    return isset($this->config['tasks'][$name]);
  }

  /**
   * Attempt to get a defined task by name.
   *
   * @param string $name
   *   The name of the task to return.
   *
   * @return DevGenTask
   *   The matching devgen task, otherwise NULL.
   */
  final public function get($name) {
    return $this->has($name) ? $this->config['tasks'][$name] : NULL;
  }

  /**
   * Deletes the given task.
   *
   * @param string $name
   *   The name of the task to delete.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function del($name) {
    unset($this->config['tasks'][$name]);

    if ($this->getSetting('entry_point') == $name) {
      $this->setSetting('entry_point', NULL);
    }

    return $this;
  }

  /**
   * Saves the devgen.
   *
   * @return bool|int
   *   The result of drupal_write_record.
   */
  final public function save() {
    return devgen_save($this->name, $this->export());
  }

  /**
   * Execute a specific task, this will also execute any exit points.
   *
   * @param string $name
   *   The task to execute.
   * @param bool $evaluate
   *   (optional = FALSE) TRUE to evaluate the devgen, without committing
   *   any changes.
   */
  final public function executeTask($name, $evaluate = FALSE) {
    if ($this->output) {
      $this->start(t('@state task "@name".', array(
        '@state' => $evaluate ? t('Evaluating') : t('Executing'),
        '@name' => $name,
      )));
    }

    if (!isset($this->config['tasks'][$name])) {
      if ($this->output) {
        $this->message(t('Failed to find a task with the name "@name".', array(
          '@name' => $name,
        )), 'error');
      }
    }
    else {
      $this->config['tasks'][$name]->execute($evaluate);
    }

    if ($this->output) {
      $this->stop();
    }
  }

  /**
   * Executes this devgen.
   *
   * @param bool $evaluate
   *   (optional = FALSE) TRUE to evaluate the devgen, without committing
   *   any changes.
   *
   * @return bool
   *   Returns the success state.
   */
  final public function execute($evaluate = FALSE) {
    $success = TRUE;

    $this->runId = time();
    $running = &drupal_static('devgen_running');
    if (isset($running)) {
      $old_running = $running;
    }
    $running = $this;

    $this->output = new DevGenOutput();
    $this->start(t('Executing devgen "@name".', array(
      '@name' => $this->getName(),
    )));

    if (count($this->config['tasks'])) {
      $entry_point = $this->getSetting('entry_point');
      $loops = max((int) $this->getSetting('loops', 1), 1);
      for ($i = 1; $i <= $loops; $i++) {
        foreach ($this->config['properties'] as $property) {
          /* @var DevGenProperty $property */
          $property->reset();
        }

        $this->start(t('Starting loop #@loop...', array('@loop' => $i)));
        $this->executeTask($entry_point, $evaluate);
        $this->stop();
      }
    }

    while ($this->output->getParent() !== NULL) {
      $this->output = $this->output->stop();
    }

    module_invoke_all('devgen_post_execute', $this, $evaluate);

    $this->output->stop();

    $running = isset($old_running) ? $old_running : FALSE;
    $this->runId = FALSE;

    return $success;
  }

  /**
   * Get the current output stack.
   *
   * @return DevGenOutput
   *   Returns the current output stack.
   */
  final public function getOutput() {
    return $this->output;
  }

  /**
   * Start a new output stack.
   *
   * @param string $message
   *   (optional = NULL) Add a message to the current stack before starting the
   *   new stack.
   */
  final public function start($message = NULL) {
    $this->output = $this->output->start($message);
  }

  /**
   * Send a message to the current output stack.
   *
   * @param string $message
   *   The message to add to the output stack.
   */
  final public function message($message, $status = 'info') {
    $this->output->message($message, $status);
  }

  /**
   * Stop the current output stack.
   */
  final public function stop() {
    $this->output = $this->output->stop();
  }

  /**
   * Imports a devgen config array and builds up the object.
   *
   * @param array $config
   *   The devgen config array to build.
   *
   * @return DevGen
   *   Returns self.
   */
  final public function import(array $config) {
    if (isset($config['properties']) && count($config['properties'])) {
      foreach ($config['properties'] as $name => $info) {
        $config['properties'][$name] = devgen_property($this, $name, $info['name'], $info['value'], $info['config'], FALSE);
      }
    }

    if (isset($config['tasks']) && count($config['tasks'])) {
      foreach ($config['tasks'] as $name => $info) {
        $config['tasks'][$name] = devgen_task($this, $name, $info['name'], $info['config'], NULL, FALSE);
      }

      uasort($config['tasks'], function ($a, $b) {
        if ($a->weight() === $b->weight()) {
          return 0;
        }

        return $a->weight() < $b->weight() ? -1 : 1;
      });
    }

    drupal_alter('devgen_import', $config);
    $this->config = $config;
    return $this;
  }

  /**
   * Export the devgen config array.
   *
   * @return array
   *   Returns the exported config array.
   */
  final public function export() {
    $config = $this->config;

    if (isset($config['properties']) && count($config['properties'])) {
      foreach ($config['properties'] as $name => $property) {
        /* @var DevGenProperty $property */
        $config['properties'][$name] = $property->export();
      }
    }

    if (isset($config['tasks']) && count($config['tasks'])) {
      foreach ($config['tasks'] as $name => $task) {
        /* @var DevGenTask $task */
        $config['tasks'][$name] = $task->export();
      }
    }

    unset($config['_storage']);

    drupal_alter('devgen_import', $config);
    return $config;
  }

}
