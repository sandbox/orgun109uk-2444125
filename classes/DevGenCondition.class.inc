<?php
/**
 * @file
 * Provides a condition class.
 */

/**
 * The DevGenCondition class.
 */
abstract class DevGenCondition extends DevGenTask {

  /**
   * Executes the condition.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function execute($evaluate = FALSE) {
    $this->devgen->start(t('Executing the condition...'));
    try {
      $success = $this->onExecute($evaluate);
      if ($this->getSetting('negate', FALSE)) {
        $success = !$success;
      }

      module_invoke_all('devgen_condition_execute', $this, array(&$success));
    }
    catch (Exception $e) {
      $this->devgen->message(t('The task has @state', array(
        '@state' => t('failed'),
      )));
      $success = FALSE;
    }

    $this->devgen->message(t('The condition state has @state', array(
      '@state' => $success ? t('passed') : t('failed'),
    )));

    if ($success) {
      $success_exit_point = $this->getSetting('success_exit_point');
      if ($success_exit_point) {
        $this->devgen->executeTask($success_exit_point, $evaluate);
      }
    }
    else {
      $fail_exit_point = $this->getSetting('fail_exit_point');
      if ($fail_exit_point) {
        $this->devgen->executeTask($fail_exit_point, $evaluate);
      }
    }

    $this->devgen->stop();
    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function exitPoints() {
    return array(
      'success_exit_point' => array(
        'title' => t('On success'),
        'description' => t('The successful exit point, ie. which task to execute on success.'),
      ),
      'fail_exit_point' => array(
        'title' => t('On fail'),
        'description' => t('The fail exit point, ie. which task to execute on failure.'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['negate'] = array(
      '#type' => 'checkbox',
      '#title' => t('Negate'),
      '#description' => t('If selected, the success result will be reveresed.'),
      '#default_value' => $this->getSetting('negate', FALSE),
    );

    return $form;
  }

}
