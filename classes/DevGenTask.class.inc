<?php
/**
 * @file
 * Provides a base task class.
 */

/**
 * The DevGenTask class.
 */
abstract class DevGenTask {

  /**
   * The owning devgen.
   *
   * @var DevGen
   */
  protected $devgen;

  /**
   * The name of the task.
   *
   * @var string
   */
  protected $name;

  /**
   * Config given to the task.
   *
   * @var array
   */
  protected $config;

  /**
   * Called internally to set the config.
   *
   * This allows task classes to override and put in any pre/post processing.
   *
   * @param array $config
   *   The imported config array.
   */
  protected function importConfig(array $config) {
    $this->config = $config;
  }

  /**
   * Called internally to get the exportable config.
   *
   * This allows task classes to override and put in any pre/post processing.
   *
   * @return array
   *   The exportable config array.
   */
  protected function exportConfig() {
    return $this->config;
  }

  /**
   * The task constructor.
   *
   * @param DevGen $devgen
   *   The owning devgen class.
   * @param string $name
   *   The name of the task.
   * @param array $config
   *   (optional = []) The config to give to the task.
   */
  public function __construct(DevGen $devgen, $name, array $config = array()) {
    $this->devgen = $devgen;
    $this->name = $name;
    $this->importConfig($config);
  }

  /**
   * Export this task.
   *
   * @return array
   *   The exported data.
   */
  final public function export() {
    return array(
      'name' => devgen_task_to_type(get_class($this)),
      'config' => $this->exportConfig(),
    );
  }

  /**
   * {@inheritdoc}
   */
  final public function __sleep() {
    $this->config = $this->exportConfig();
    return array('name', 'config');
  }

  /**
   * {@inheritdoc}
   */
  final public function __wakeup() {
    $this->importConfig($this->config);
  }

  /**
   * {@inheritdoc}
   */
  final public function __toString() {
    return json_encode($this->export(), JSON_PRETTY_PRINT);
  }

  /**
   * Get the name of this task.
   *
   * @return string
   *   The name of this task.
   */
  final public function getName() {
    return $this->name;
  }

  /**
   * Get the weight of the task.
   *
   * @return int
   *   The weight of this task.
   */
  final public function weight() {
    return isset($this->config['weight']) ? (int) $this->config['weight'] : 0;
  }

  /**
   * Executes the task, pre and post execute hooks are also invoked.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  abstract public function execute($evaluate = FALSE);

  /**
   * Called just prior to the execution of the task.
   *
   * This allows for any pre-execution tasks to be fulfilled.
   */
  protected function onPreExecute() {
    // Does nothing.
  }

  /**
   * Execute the given task.
   *
   * Any specific properties given to this task are accessible from the get
   * method, access to global properties are accessible using the devgen get
   * and set methods.
   *
   * @return bool
   *   Returns TRUE on success.
   *
   * @throws Exception
   *   An exception is thrown if there's any problems and will be caught by
   *   the devgen to log and display.
   */
  abstract protected function onExecute($evaluate = FALSE);

  /**
   * Called just after to the execution of the task.
   *
   * This allows for any post-execution tasks to be fulfilled.
   */
  protected function onPostExecute() {
    // Does nothing.
  }

  /**
   * Get the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $default
   *   (optional = NULL) The default value to return if the setting does'nt
   *   exist.
   * @param bool $parse
   *   (optional = FALSE) Set to TRUE to parse through the token parser.
   *
   * @return mixed
   *   Returns the setting value or the $default.
   */
  final public function getSetting($setting, $default = NULL, $parse = FALSE) {
    $value = $this->hasSetting($setting) ? $this->config[$setting] : $default;
    if ($parse) {
      $value = $this->devgen->parseTokens($value);
    }

    return $value;
  }

  /**
   * Set the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $value
   *   The value to apply to the setting.
   *
   * @return DevGenTask
   *   Returns self.
   */
  final public function setSetting($setting, $value) {
    $this->config[$setting] = $value;
    return $this;
  }

  /**
   * Determine if a setting has been supplied.
   *
   * @param string $setting
   *   The setting name.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function hasSetting($setting) {
    return isset($this->config[$setting]);
  }

  /**
   * Add an entry point field.
   *
   * @param string $name
   *   The name of the setting.
   * @param string $title
   *   The title to apply to the field.
   * @param string $description
   *   The description to apply to the field.
   *
   * @return array
   *   A drupal select field.
   */
  final private function exitPointField($name, $title, $description) {
    return array(
      '#type' => 'select',
      '#title' => $title,
      '#description' => $description,
      '#options' => $this->devgen->getTaskOptions(),
      '#default_value' => $this->getSetting($name),
      '#attributes' => array(
        'class' => array('devgen-task-options'),
      ),
    );
  }

  /**
   * Define all the available exit points provided by this task.
   *
   * @return array
   *   An array of exit point arrays indexed by the exit point name with a
   *   title and description.
   */
  abstract public function exitPoints();

  /**
   * Define the properties UI settings form.
   *
   * @param array $values
   *   The values being passed, this will only be available during an AJAX
   *   rebuild.
   *
   * @return array
   *   The form elements.
   */
  public function settings(array $values = NULL) {
    $form = array();

    $exit_points = $this->exitPoints();
    if (count($exit_points)) {
      $form['exit_points'] = array(
        '#type' => 'fieldset',
        '#title' => t('Exit points'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      foreach ($exit_points as $name => $exit_point) {
        $form['exit_points'][$name] = $this->exitPointField($name, $exit_point['title'], $exit_point['description']);
      }
    }

    $form['weight'] = array(
      '#type' => 'textfield',
      '#title' => t('Weight'),
      '#default_value' => $this->weight(),
    );

    return $form;
  }

  /**
   * Validates the submitted form settings.
   *
   * @param array $settings
   *   An array of settings that have been submitted.
   * @param array $errors
   *   A reference to an errors array, used to add any errors with the
   *   validation, and is indexed by the fields name.
   *
   * @return bool
   *   Returns TRUE of FALSE.
   */
  public function validateSettings(array $settings, array &$errors) {
    if (!empty($settings['weight']) && !is_numeric($settings['weight'])) {
      $errors['weight'] = t('The weight must be a numeric value.');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * The submitted form settings.
   *
   * @param array $settings
   *   An array of settings that have been submitted.
   */
  public function submitSettings(array $settings) {
    $exit_points = isset($settings['exit_points']) ? $settings['exit_points'] : array();
    unset($settings['exit_points']);

    foreach ($settings as $setting => $value) {
      $this->setSetting($setting, $value);
    }

    foreach ($exit_points as $exit_point => $value) {
      $this->setSetting($exit_point, $value);
    }
  }

}
