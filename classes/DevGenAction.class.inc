<?php
/**
 * @file
 * Provides the action class.
 */

/**
 * The DevGenAction class.
 */
abstract class DevGenAction extends DevGenTask {

  /**
   * An array of properties this task provides.
   *
   * @var array
   */
  protected $properties;

  /**
   * An array of property values.
   *
   * @var array
   */
  protected $propertyValues;

  /**
   * {@inheritdoc}
   */
  public function __construct(DevGen $devgen, $name, array $config = array()) {
    parent::__construct($devgen, $name, $config);
    $this->resetProperties();
  }

  /**
   * Allows tasks to define the default properties array.
   *
   * @return array
   *   The default properties and description.
   */
  protected function onResetProperties() {
    return array(
      'success' => 'Success state',
    );
  }

  /**
   * Internally resets the properties data.
   *
   * @return DevGenTask
   *   Returns self.
   */
  final protected function resetProperties() {
    $this->properties = $this->onResetProperties();
    foreach (array_keys($this->properties) as $property) {
      $this->propertyValues[$property] = '';
    }

    return $this;
  }

  /**
   * Get an array of property names.
   *
   * @return array
   *   An array of property names.
   */
  final public function getPropertiesNames() {
    return array_keys($this->properties);
  }

  /**
   * Returns the properties in the form of tokens.
   *
   * @param bool $parse
   *   (optional = FALSE) If TRUE the values of the tokens will be the task
   *   property values.
   *
   * @return array
   *   Returns the tokens.
   */
  final public function getTokens($parse = FALSE) {
    $tokens = array();
    foreach ($this->properties as $property => $title) {
      $tokens['@{' . $this->name . ':' . $property . '}'] = $parse ? $this->propertyValues[$property] : "{$this->name}->{$property}: {$title}";
    }

    return $tokens;
  }

  /**
   * Set the value of a given property.
   *
   * @param string $name
   *   The name of the property to set.
   * @param mixed $value
   *   The value to assign the property.
   *
   * @return DevGenTask
   *   Returns self.
   */
  final protected function setProperty($name, $value) {
    if ($this->hasProperty($name)) {
      $this->propertyValues[$name] = $value;
    }

    return $this;
  }

  /**
   * Get the value of a property.
   *
   * @param string $name
   *   The name of the property.
   * @param mixed $default
   *   (optional = NULL) The default value to return if the property does'nt
   *   exist.
   *
   * @return mixed
   *   The value of the property or $default.
   */
  final public function getProperty($name, $default = NULL) {
    return $this->hasProperty($name) ? $this->propertyValues[$name] : $default;
  }

  /**
   * Determines if a property has been defined.
   *
   * @param string $name
   *   The name of the property.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function hasProperty($name) {
    return isset($this->properties[$name]);
  }

  /**
   * Executes the task, pre and post execute hooks are also invoked.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function execute($evaluate = FALSE) {
    $this->resetProperties();

    $this->devgen->start(t('Pre-execute the task...'));
    module_invoke_all('devgen_operation_pre_execute', $this);
    $this->devgen->stop();

    $this->devgen->start(t('Executing the task...'));
    $success = $this->onExecute($evaluate);
    module_invoke_all('devgen_operation_execute', $this, array(&$success));
    $this->setProperty('success', $success);
    $this->devgen->stop();

    $this->devgen->start(t('Post-execute the task...'));
    module_invoke_all('devgen_operation_post_execute', $this);
    $this->devgen->stop();

    if ($success) {
      $success_exit_point = $this->getSetting('success_exit_point');
      if ($success_exit_point) {
        $this->devgen->executeTask($success_exit_point, $evaluate);
      }
    }
    else {
      $fail_exit_point = $this->getSetting('fail_exit_point');
      if ($fail_exit_point) {
        $this->devgen->executeTask($fail_exit_point, $evaluate);
      }
    }

    $this->devgen->message(t('The task has @state', array(
      '@state' => $success ? t('passed') : t('failed'),
    )), $success ? 'passed' : 'failed');
    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function exitPoints() {
    return array(
      'success_exit_point' => array(
        'title' => t('On success'),
        'description' => t('The successful exit point, ie. which task to execute on success.'),
      ),
      'fail_exit_point' => array(
        'title' => t('On fail'),
        'description' => t('The fail exit point, ie. which task to execute on failure.'),
      ),
    );
  }

}
