<?php
/**
 * @file
 * Defines the devgen output class.
 */

/**
 * Class DevGenOutput.
 */
class DevGenOutput {

  /**
   * The parent devgen output class.
   *
   * @var DevGenOutput
   */
  protected $parent;

  /**
   * The start microtime.
   *
   * @var float
   */
  protected $startTime;

  /**
   * The end microtime.
   *
   * @var float
   */
  protected $stopTime;

  /**
   * An array of children devgen output classes.
   *
   * @var array<DevGenOutput>
   */
  protected $outputs;

  /**
   * An array of messages.
   *
   * @var array<string>
   */
  protected $messages;

  /**
   * The devgen output class constructor.
   *
   * @param DevGenOutput $parent
   *   (optional = NULL) The parent devgen output class.
   */
  public function __construct(DevGenOutput $parent = NULL) {
    $this->startTime = microtime(TRUE);

    $this->parent = $parent;
    $this->outputs = array();
    $this->messages = array();
  }

  /**
   * Gets the parent devgen output class.
   *
   * @return DevGenOutput
   *   The parent output class.
   */
  final public function getParent() {
    return $this->parent;
  }

  /**
   * Adds a new message.
   *
   * @param string $message
   *   The message to add.
   */
  final public function message($message, $status = 'info') {
    $this->messages[] = array(
      'time' => round(microtime(TRUE) - $this->startTime, 3),
      'message' => $message,
      'status' => $status,
    );
  }

  /**
   * Start a new output stack.
   *
   * @param string $message
   *   (optional = NULL) A message to apply to this stack prior to starting the
   *   new stack.
   *
   * @return DevGenOutput
   *   The newly created child output stack.
   */
  final public function start($message = NULL) {
    if ($message) {
      $this->message($message);
    }

    return $this->add(new DevGenOutput($this));
  }

  /**
   * Add a output object to the outputs array.
   *
   * @param DevGenOutput $output
   *   The output object to add.
   *
   * @return DevGenOutput
   *   Returns the added output object.
   */
  final public function add(DevGenOutput $output) {
    $this->outputs[] = $output;
    $this->messages[] = '@@' . (count($this->outputs) - 1);

    return $output;
  }

  /**
   * Stop the current output stack.
   *
   * @return DevGenOutput
   *   Returns the parent output stack.
   */
  final public function stop() {
    $this->stopTime = microtime(TRUE);

    $seconds = round($this->stopTime - $this->startTime, 3);
    $this->message(t('Completed in @seconds seconds.', array(
      '@seconds' => $seconds,
    )));

    return $this->parent;
  }

  /**
   * Generates an output array from the output stack.
   *
   * @return array
   *   An array of messages.
   */
  final public function output() {
    $output = array();

    foreach ($this->messages as $message) {
      if (is_string($message) && strpos($message, '@@') === 0) {
        $idx = substr($message, 2);
        $output[] = $this->outputs[$idx]->output();
      }
      else {
        $output[] = $message;
      }
    }

    return $output;
  }

}
