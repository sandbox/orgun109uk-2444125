<?php
/**
 * @file
 * Provides the property class.
 */

/**
 * The DevGenProperty class.
 */
abstract class DevGenProperty {

  /**
   * The owning devgen.
   *
   * @var DevGen
   */
  protected $devgen;

  /**
   * The name of the task.
   *
   * @var string
   */
  protected $name;

  /**
   * Config given to the task.
   *
   * @var array
   */
  protected $config;

  /**
   * The task constructor.
   *
   * @param DevGen $devgen
   *   The owning devgen class.
   * @param string $name
   *   The name of this property.
   * @param mixed $value
   *   (optional = NULL) The initial value to apply to this property.
   * @param array $config
   *   (optional = []) The config to give to the task.
   */
  final public function __construct(DevGen $devgen, $name, $value = NULL, array $config = array()) {
    $this->devgen = $devgen;
    $this->name = $name;
    $this->value = $value;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  final public function __toString() {
    return json_encode($this->export(), JSON_PRETTY_PRINT);
  }

  /**
   * Export this property.
   *
   * @return array
   *   The exported data.
   */
  final public function export() {
    return array(
      'name' => devgen_property_to_type(get_class($this)),
      'value' => $this->value,
      'config' => $this->config,
    );
  }

  /**
   * Get the name of this task.
   *
   * @return string
   *   The name of this task.
   */
  final public function getName() {
    return $this->name;
  }

  /**
   * Reset the value, used on dynamically generated properties.
   */
  public function reset() {
    // Does nothing.
  }

  /**
   * Returns the raw value of this property.
   *
   * @return mixed
   *   Returns the value.
   */
  final public function rawValue() {
    return $this->value;
  }

  /**
   * Returns the value of this property (before processing).
   *
   * @return mixed
   *   Returns the value.
   */
  protected function onValue() {
    return $this->rawValue();
  }

  /**
   * Returns the value of this property (after processing).
   *
   * @return mixed
   *   Returns the value.
   */
  final public function value() {
    return $this->onValue();
  }

  /**
   * Get the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $default
   *   (optional = NULL) The default value to return if the setting does'nt
   *   exist.
   * @param bool $parse
   *   (optional = FALSE) Set to TRUE to parse through the token parser.
   *
   * @return mixed
   *   Returns the setting value or the $default.
   */
  final public function getSetting($setting, $default = NULL, $parse = FALSE) {
    $value = $this->hasSetting($setting) ? $this->config[$setting] : $default;
    if ($parse) {
      $value = $this->devgen->parseTokens($value);
    }

    return $value;
  }

  /**
   * Set the specified config setting.
   *
   * @param string $setting
   *   The setting name.
   * @param mixed $value
   *   The value to apply to the setting.
   *
   * @return DevGenProperty
   *   Returns self.
   */
  final public function setSetting($setting, $value) {
    $this->config[$setting] = $value;
    return $this;
  }

  /**
   * Determine if a setting has been supplied.
   *
   * @param string $setting
   *   The setting name.
   *
   * @return bool
   *   Returns TRUE or FALSE.
   */
  final public function hasSetting($setting) {
    return isset($this->config[$setting]);
  }

  /**
   * Define the properties UI settings form.
   *
   * @return array
   *   The form elements.
   */
  public function settings() {
    return array();
  }

  /**
   * Validates the submitted form settings.
   *
   * @param array $settings
   *   An array of settings that have been submitted.
   * @param array $errors
   *   A reference to an errors array, used to add any errors with the
   *   validation, and is indexed by the fields name.
   *
   * @return bool
   *   Returns TRUE of FALSE.
   */
  public function validateSettings(array $settings, array &$errors) {
    return TRUE;
  }

  /**
   * The submitted form settings.
   *
   * @param array $settings
   *   An array of settings that have been submitted.
   */
  public function submitSettings(array $settings) {
    // Does nothing.
  }

}
