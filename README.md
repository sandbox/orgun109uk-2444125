# DevGen

A Drupal module that allows an automated method of performing actions and conditions.

## Installation

Enable the core module via Drush:

```sh
drush pm-enable -y devgen
```

The UI for DevGen is supplied by an additional module:

```sh
drush pm-enable -y devgen_ui
```

## Example

An example module is provided which provides an example DevGen config which creates a batch of 10 users with
a random username, and displays a drupal message.

```sh
drush pm-enable -y devgen_example
```

## Plugins

DevGen is powered by 2 main types of plugins, *Properties* and *Tasks*. These plugins are defined by firstly creating
the plugin class and then defining it using the *hook_devgen_properties* or *hook_devgen_tasks*.

### Properties

Properties allow storing property data within the config, in the example this is a randomly generated string for the
users username.

### Tasks

Tasks perform, as the name suggests given tasks. Each task will have an exit point (or multiple) depending on the
results of the task. For instance the verify user condition as two exit points, one for successfully finding the user
and one for failing to find a matching user.

There are two types of task classes, *Conditions* and *Actions*.

#### Conditions

Conditions will normally have two exit points and are used to decide which route to take through the execution of the
config.

#### Actions

Actions can have exit points or may not, and these are used to perform actions such as creating a user account, or
add a message to the watchdog.
