(function ($) {
  'use strict';

  Drupal.behaviors.devgenUI = {

    attach: function (context, settings) {
      if (context === document) {
        $(document)
            .delegate('#devgen-ui-view', 'updateEntryPoints', function (e, data) {
              var options = [];
              for (var val in data) {
                options.push($('<option>').attr('value', val).html(data[val])[0].outerHTML);
              }

              $('select.devgen-task-options.form-select').each(function (index, item, items) {
                var $item = $(item),
                    current = $item.val();
                $item.html(options.join('')).val(current);
              });
            });
      }
    }

  };

})(jQuery);
