(function ($) {
    'use strict';

    var activePopup = null,
        $overlay = null;

    function devgenUIShowTaskConfig(name) {
        var $element = $('#task-container--' + name);
        console.info(name, $element);
        if ($element.length && $element.hasClass('devgenui-popup') === false) {
            activePopup = name;

            $element.addClass('devgenui-popup');

            if ($overlay === null) {
                $overlay = $('<div>').addClass('overlay');
                $overlay
                    .insertBefore($element)
                    .click(function () {
                        $('#container-tasks > .task-container').removeClass('devgenui-popup');
                        $overlay.remove();

                        activePopup = null;
                        $overlay = null;
                    });
            }
        }
    }

    var basicType = {
            connector: 'StateMachine',
            paintStyle: {strokeStyle: 'red', lineWidth: 4},
            hoverPaintStyle: {strokeStyle: 'blue'},
            overlays: ['Arrow']
        },
        sourceEndpoint = {
            endpoint: 'Dot',
            paintStyle: {fillStyle: '#61B7CF', radius: 4},
            maxConnections: -1,
            isSource: true,
            connector: ['Flowchart', {stub: [30, 30], gap: 4, cornerRadius: 2, alwaysRespectStubs: true}],
            connectorStyle: {
                lineWidth: 4,
                strokeStyle: '#61B7CF',
                joinstyle: 'round'
            }
        },
        targetEndpoint = {
            endpoint: 'Dot',
            paintStyle: {fillStyle: '#7AB02C', radius: 4},
            maxConnections: -1,
            isTarget: true
        };

    function setupEndPoints(taskCard) {
        window.devgenUIInstance.addEndpoint('task-card--' + taskCard, targetEndpoint, {
            anchor: [0.5, 0, 0, -1],
            uuid: taskCard + ':in'
        });

        window.devgenUIInstance.addEndpoint('task-card--' + taskCard, sourceEndpoint, {
            anchor: [0.5, 1, 0, 1],
            uuid: taskCard + ':out'
        });
    }

    function setupCoreEndPoints(id, hasIn, hasOut) {
        if (hasIn) {
            window.devgenUIInstance.addEndpoint('task-card--' + id, targetEndpoint, {
                anchor: [0.5, 0, 0, -1],
                uuid: id + ':in'
            });
        }

        if (hasOut) {
            window.devgenUIInstance.addEndpoint('task-card--' + id, sourceEndpoint, {
                anchor: [0.5, 1, 0, 1],
                uuid: id + ':out'
            });
        }
    }

    function reconnect() {
        window.devgenUIInstance.reset();
        buildUML();
    }

    window.devgenUIReconnect = reconnect;

    function buildUML() {
        window.devgenUIInstance.batch(function () {
            var $elements = $('#container-tasks-cards-sheet .task-card[attr-task]');
            window.devgenUIInstance.draggable($elements, {
                containment: '#container-tasks-cards-sheet',
                grid: [10, 10],
                stop: function (e, ui) {
                    var $this = $(this),
                        pos = $this.position();

                    $(this).find('input[type="hidden"][attr-target="top"]').val(pos.top);
                    $(this).find('input[type="hidden"][attr-target="left"]').val(pos.left);
                }
            });

            $('#container-tasks-cards-sheet .task-card')
                .each(function (index, item) {
                    var $element = $(item),
                        id = $element.attr('attr-task');

                    if (!$element.hasClass('task-card-core')) {
                        setupEndPoints(id);
                    } else {
                        setupCoreEndPoints(id, parseInt($element.attr('attr-has-in')) === 1, parseInt($element.attr('attr-has-out')) === 1);
                    }
                })
                .each(function (index, item) {
                    var $element = $(item),
                        id = $element.attr('attr-task'),
                        connectors = JSON.parse($element.attr('attr-connector-out'));

                    for (var connector in connectors) {
                        var overlays = [];

                        if (connectors[connector] && connectors[connector] !== '') {
                            overlays = [
                                ['Label', {location: 0.5, label: connectors[connector], cssClass: 'task-label'}]
                            ]
                        }

                        window.devgenUIInstance.connect({
                            uuids: [id + ':out', connector + ':in'],
                            editable: false,
                            overlays: overlays
                        });
                    }
                });
        });
    }

    Drupal.behaviors.devgenUI = {

        attach: function (context, settings) {
            if (activePopup && $(context).attr('id') === 'task-container--' + activePopup) {
                devgenUIShowTaskConfig(activePopup);
            }

            if (context === document) {
                $(document)
                    .delegate('.task-card[attr-task]', 'dblclick', function (e) {
                        e.preventDefault();

                        var task = $(this).attr('attr-task');
                        devgenUIShowTaskConfig(task);
                    });

                $('#container-tasks').css({
                    'visibility': 'hidden',
                    'overflow': 'hidden',
                    'height': 0
                });

                jsPlumb.ready(function () {
                    window.devgenUIInstance = jsPlumb.getInstance({
                        DragOptions: {cursor: 'pointer', zIndex: 2000},
                        ConnectionOverlays: [
                            ['Arrow', {location: 1}],
                            ['Label', {location: 0.1, id: 'label', cssClass: 'aLabel'}]
                        ],
                        Container: 'container-tasks-cards'
                    });

                    window.devgenUIInstance.registerConnectionType('basic', basicType);
                    buildUML();
                });
            } else {
                reconnect();
            }
        }

    };

})(jQuery);
