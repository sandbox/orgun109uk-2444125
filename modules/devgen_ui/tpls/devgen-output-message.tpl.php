<?php
/**
 * @file
 * DevGen output message template.
 *
 * @param DevGen $devgen
 *   The DevGen object.
 * @param float $time
 *   The time from the start-time.
 * @param string $message
 *   The message.
 * @param string $status
 *   The message status.
 */
?>
<li class="devgen-output-message devgen-output-status--<?php print $status; ?>">
  <span class="devgen-msg-time" title="<?php print t('@time seconds', array('@time' => $time)); ?>"><?php print $time; ?></span>
  <span class="devgen-msg"><?php print $message; ?></span>
</li>
