<?php
/**
 * @file
 * DevGen output messages template.
 *
 * @param DevGen $devgen
 *   The DevGen object.
 * @param array $output
 *   The output array.
 * @param array $class
 *   An array of CSS classes.
 */

  $classes = (is_array($class) && count($class)) ? implode(' ', $class) : '';
?>
<ul class="<?php print $classes; ?>">
  <?php foreach ($output as $item) : ?>
  <?php if (is_array($item) && isset($item['message'])) : ?>
  <?php print theme('devgen_output_message', array(
    'devgen' => $devgen,
    'time' => $item['time'],
    'message' => $item['message'],
    'status' => $item['status'],
  )); ?>
  <?php else : ?>
  <li class="devgen-output-items"><?php print theme('devgen_output_messages', array(
    'devgen' => $devgen,
    'output' => $item,
    'class' => array('devgen-items'),
  )); ?></li>
  <?php endif; ?>
  <?php endforeach; ?>
</ul>
