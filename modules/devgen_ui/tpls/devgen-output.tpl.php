<?php
/**
 * @file
 * DevGen output template.
 *
 * @param DevGen $devgen
 *   The DevGen object.
 */
?>
<div class="devgen-output-container">
  <?php print theme('devgen_output_messages', array(
    'devgen' => $devgen,
    'output' => $devgen->getOutput()->output(),
    'class' => array('devgen-output', 'devgen-items'),
  )); ?>
</div>
