<?php
/**
 * @file
 * A form callback to view the specified devgen.
 */



function _devgen_ui_form_tokens(&$form, $devgen) {
  $form['tokens'] = array(
    '#prefix' => '<div id="current-tokens">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#title' => t('Tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'tokens' => array(
      '#theme' => 'item_list',
      '#items' => array(),
    ),
  );

  foreach ($devgen->propertyTokens() as $token => $label) {
    $form['tokens']['tokens']['#items'][] = "<strong>{$token}</strong> - {$label}";
  }
}

function _devgen_ui_form_task(&$form, $devgen, $name, $values) {
  $task = $devgen->get($name);
  $task_type = $task instanceof DevGenAction ? 'action' : 'condition';
  $task_name = devgen_task_to_type(get_class($task));

  $form[$name] = array(
    '#type' => 'fieldset',
    '#title' => t('[@type:@task] - @name', array(
      '@type' => $task_type,
      '@task' => devgen_task_title($task_name),
      '@name' => $name,
    )),
    '#prefix' => "<div id=\"task-container--{$name}\" class=\"task-container\">",
    '#suffix' => '</div>',
    '#description' => devgen_task_description($task_name),
    '#collapsible' => TRUE,
    '#collapsed' => $values === NULL,
  );

  $form[$name]['properties'] = $task->settings($values);

  $form[$name]['update'] = array(
    '#type' => 'submit',
    '#name' => "update-task--{$name}",
    '#value' => t('Update'),
    '#ajax' => array(
      'wrapper' => "task-container--{$name}",
      'callback' => 'ajax_devgen_ui_view_update_task',
    ),
    '#validate' => array('_devgen_ui_view_task_validate'),
    '#submit' => array('_devgen_ui_view_task_update'),
    '#limit_validation_errors' => array(
      array('tasks', 'container', $name, 'properties'),
    ),
  );

  $form[$name]['remove'] = array(
    '#type' => 'submit',
    '#name' => "remove-task--{$name}",
    '#value' => t('Remove'),
    '#ajax' => array(
      'wrapper' => "task-container--{$name}",
      'callback' => 'ajax_devgen_ui_view_remove_task',
    ),
    '#submit' => array('_devgen_ui_view_task_remove'),
    '#limit_validation_errors' => array(),
  );
}

function _devgen_ui_form_add_task(&$form) {
  $form['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new task'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#prefix' => '<div id="new-task">',
    '#suffix' => '</div>',
    '#weight' => 10,
  );

  $form['new']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
  );

  $form['new']['task'] = array(
    '#type' => 'select',
    '#title' => t('Task'),
    '#options' => array_merge(
      array('' => t('- Select a task -')),
      devgen_ui_tasks()
    ),
  );

  $form['new']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add new task'),
    '#ajax' => array(
      'wrapper' => 'new-task',
      'callback' => 'ajax_devgen_ui_view_add_task',
    ),
    '#validate' => array('_devgen_ui_view_add_task_validate'),
    '#submit' => array('_devgen_ui_view_add_task_submit'),
    '#limit_validation_errors' => array(
      array('tasks', 'new'),
    ),
  );
}




/**
 * The main devgen view form callback.
 */
function devgen_ui_view($form, &$form_state, $name = NULL) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $devgen = devgen_new($name, $config);
  $form_state['devgen'] = $devgen;

  drupal_add_library('system', 'ui.draggable');
  $path = drupal_get_path('module', 'devgen_ui');

  $form['#attached']['css'] = array(
    "{$path}/assets/css/devgen-ui.css",
  );

  $form['#attached']['js'] = array(
    "{$path}/assets/js/devgen-ui.js",
  );

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'general',
  );

  $form['general'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('General details'),
    '#group' => 'tabs',
  );

  $form['general']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $form_state['devgen']->getTitle(),
  );

  $form['general']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $form_state['devgen']->getDescription(),
  );

  $created_on = $form_state['devgen']->createdOn();
  $form['general']['created'] = array(
    '#type' => 'item',
    '#title' => t('Created'),
    '#markup' => !empty($created_on) ? t('on @date by @user', array(
      '@date' => format_date($created_on),
      '@user' => $form_state['devgen']->createdBy(),
    )) : '-',
  );

  $modified_on = $form_state['devgen']->modifiedOn();
  $form['general']['modified'] = array(
    '#type' => 'item',
    '#title' => t('Modified'),
    '#markup' => !empty($modified_on) ? t('on @date by @user', array(
      '@date' => format_date($modified_on),
      '@user' => $form_state['devgen']->modifiedBy(),
    )) : '-',
  );

  $form['settings'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#group' => 'tabs',
  );

  $form['settings']['entry_point'] = array(
    '#type' => 'select',
    '#title' => t('Entry point'),
    '#description' => t('This is the first task that gets executed.'),
    '#options' => $form_state['devgen']->getTaskOptions(),
    '#default_value' => $form_state['devgen']->getSetting('entry_point'),
    '#attributes' => array(
      'class' => array('devgen-task-options'),
    ),
  );

  $form['settings']['loops'] = array(
    '#type' => 'textfield',
    '#title' => t('Loops'),
    '#description' => t('The number of loops to execute.'),
    '#default_value' => $form_state['devgen']->getSetting('loops', 1),
  );

  drupal_alter('devgen_ui_settings', $form_state['devgen'], $form['settings']);

  _devgen_ui_view_properties($form, $form_state);
  _devgen_ui_view_tasks($form, $form_state);

  $form['actions'] = array('#type' => 'actions', '#attributes' => array('style' => 'clear:both;'));
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#validate' => array('_devgen_ui_view_validate'),
    '#submit' => array('_devgen_ui_view_submit'),
    '#limit_validation_errors' => array(
      array('general'),
      array('settings'),
    ),
  );

  return $form;
}

/**
 * Helper to add the properties to the main form.
 */
function _devgen_ui_view_properties(&$form, &$form_state) {
  $form['properties'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Properties'),
    '#group' => 'tabs',
  );

  $form['properties']['container'] = array(
    '#prefix' => '<div id="container-properties">',
    '#suffix' => '</div>',
  );

  $properties = $form_state['devgen']->getPropertyNames();
  foreach ($properties as $name) {
    $property_name = devgen_property_to_type(get_class($form_state['devgen']->getProperty($name)));

    $form['properties']['container'][$name] = array(
      '#type' => 'fieldset',
      '#title' => t('[@property] - @name', array(
        '@property' => devgen_property_title($property_name),
        '@name' => $name,
      )),
      '#prefix' => "<div id=\"property-container--{$name}\">",
      '#suffix' => '</div>',
      '#description' => devgen_property_description($property_name),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['properties']['container'][$name]['properties'] = $form_state['devgen']->getProperty($name)->settings();
    $form['properties']['container'][$name]['update'] = array(
      '#type' => 'submit',
      '#name' => "update-property--{$name}",
      '#value' => t('Update'),
      '#ajax' => array(
        'wrapper' => "property-container--{$name}",
        'callback' => 'ajax_devgen_ui_view_update_property',
      ),
      '#validate' => array('_devgen_ui_view_property_validate'),
      '#submit' => array('_devgen_ui_view_property_update'),
      '#limit_validation_errors' => array(
        array('properties', 'container', $name),
      ),
    );

    $form['properties']['container'][$name]['remove'] = array(
      '#type' => 'submit',
      '#name' => "remove-property--{$name}",
      '#value' => t('Remove'),
      '#ajax' => array(
        'wrapper' => "property-container--{$name}",
        'callback' => 'ajax_devgen_ui_view_remove_property',
      ),
      '#submit' => array('_devgen_ui_view_property_remove'),
      '#limit_validation_errors' => array(),
    );
  }

  $form['properties']['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new property'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#prefix' => '<div id="new-property">',
    '#suffix' => '</div>',
  );

  $form['properties']['new']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
  );

  $form['properties']['new']['property'] = array(
    '#type' => 'select',
    '#title' => t('Property'),
    '#options' => array_merge(
      array('' => t('- Select a property -')),
      devgen_ui_properties()
    ),
  );

  $form['properties']['new']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add new property'),
    '#ajax' => array(
      'wrapper' => 'new-property',
      'callback' => 'ajax_devgen_ui_view_add_property',
    ),
    '#validate' => array('_devgen_ui_view_add_property_validate'),
    '#submit' => array('_devgen_ui_view_add_property_submit'),
    '#limit_validation_errors' => array(
      array('properties', 'new'),
    ),
  );
}

/**
 * Helper to add the tasks to the main form.
 */
function _devgen_ui_view_tasks(&$form, &$form_state) {
  $form['tasks'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Tasks'),
    '#group' => 'tabs',
  );

  _devgen_ui_form_tokens($form['tasks'], $form_state['devgen']);

  $form['tasks']['container'] = array(
    '#prefix' => '<div id="container-tasks">',
    '#suffix' => '</div>',
  );

  $tasks = $form_state['devgen']->getTaskNames();
  foreach ($tasks as $name) {
    $values = isset($form_state['values']) ? $form_state['values']['tasks']['container'][$name]['properties'] : NULL;
    _devgen_ui_form_task($form['tasks']['container'], $form_state['devgen'], $name, $values);
  }

  _devgen_ui_form_add_task($form['tasks']);
}

/**
 * Validate callback for the devgen view form.
 */
function _devgen_ui_view_validate($form, &$form_state) {
  if (
    !is_numeric($form_state['values']['settings']['loops']) ||
    (int) $form_state['values']['settings']['loops'] < 1
  ) {
    form_set_error('settings][loops', t('The number of loops must be a numeric value greater or equals to 1.'));
  }
}

/**
 * Submit callback for the devgen view form.
 */
function _devgen_ui_view_submit($form, &$form_state) {
  $form_state['devgen']->setTitle($form_state['values']['general']['title']);
  $form_state['devgen']->setDescription($form_state['values']['general']['description']);

  foreach ($form_state['values']['settings'] as $setting => $value) {
    $form_state['devgen']->setSetting($setting, $value);
  }

  if ($form_state['devgen']->save()) {
    drupal_set_message(t('Changes to the devgen have been saved.'));
  }
  else {
    drupal_set_message(t('There was a problem saving the devgen.'), 'warning');
  }
}

/**
 * Validate callback for the add property button.
 */
function _devgen_ui_view_add_property_validate($form, &$form_state) {
  $name = $form_state['values']['properties']['new']['name'];
  $property = $form_state['values']['properties']['new']['property'];

  if (empty($name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('The name is required.')
    );
  }
  elseif (preg_match('@[^a-z0-9_]+@', $name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('The name contains invalid characters, it must only contain a-z, 0-9 and underscore (_).')
    );
  }
  elseif ($form_state['devgen']->hasProperty($name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('This name is already being used within this context.')
    );
  }

  if (empty($property)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('property'))),
      t('You must select a property type.')
    );
  }
}

/**
 * Submit callback for the add property button.
 */
function _devgen_ui_view_add_property_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $name = $form_state['values']['properties']['new']['name'];
  $property = $form_state['values']['properties']['new']['property'];

  if ($property && devgen_property($form_state['devgen'], $name, $property, NULL, array())) {
    $form_state['devgen']->save();
  }
}

/**
 * Validate callback for the update propert button.
 */
function _devgen_ui_view_property_validate($form, &$form_state) {
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);
  $name = end($parents);

  $path = implode("']['", $parents);
  $values = NULL;
  eval("\$values = \$form_state['values']['{$path}']['properties'];");

  $errors = array();
  $form_state['devgen']->getProperty($name)->validateSettings($values, $errors);
  if (count($errors)) {
    foreach ($errors as $name => $error) {
      form_set_error(implode('][', $parents) . "][properties][{$name}", $error);
    }
  }
}

/**
 * Submit callback for the update property button.
 */
function _devgen_ui_view_property_update($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);
  $name = end($parents);

  $path = implode("']['", $parents);
  $values = NULL;
  eval("\$values = \$form_state['values']['{$path}']['properties'];");

  $form_state['devgen']->getProperty($name)->submitSettings($values);
  $form_state['devgen']->save();
}

/**
 * Submit callback for the remove property button.
 */
function _devgen_ui_view_property_remove($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);
  $name = end($parents);

  $form_state['devgen']->delProperty($name);
  $form_state['devgen']->save();
}

/**
 * Validate callback for the add task button.
 */
function _devgen_ui_view_add_task_validate($form, &$form_state) {
  $name = $form_state['values']['tasks']['new']['name'];
  $task = $form_state['values']['tasks']['new']['task'];

  if (empty($name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('The name is required.')
    );
  }
  elseif (preg_match('@[^a-z0-9_]+@', $name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('The name contains invalid characters, it must only contain a-z, 0-9 and underscore (_).')
    );
  }
  elseif ($form_state['devgen']->has($name)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('name'))),
      t('This name is already being used within this context.')
    );
  }

  if (empty($task)) {
    form_set_error(
      implode('][', array_merge(array_slice($form_state['triggering_element']['#parents'], 0, -1), array('task'))),
      t('You must select a task type.')
    );
  }
}

/**
 * Submit callback for the add task button.
 */
function _devgen_ui_view_add_task_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $name = $form_state['values']['tasks']['new']['name'];
  $task = $form_state['values']['tasks']['new']['task'];

  if ($task && devgen_task($form_state['devgen'], $name, $task, array())) {
    $form_state['devgen']->save();
  }
}

/**
 * Validate callback for the update task button.
 */
function _devgen_ui_view_task_validate($form, &$form_state) {
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -1);
  $name = end($parents);

  $path = implode("']['", $parents);
  $values = NULL;
  eval("\$values = \$form_state['values']['{$path}']['properties'];");

  $errors = array();
  $form_state['devgen']->get($name)->validateSettings($values, $errors);
  if (count($errors)) {
    foreach ($errors as $name => $error) {
      form_set_error(implode('][', $parents) . "][properties][{$name}", $error);
    }
  }
}

/**
 * Submit callback for the update task button.
 */
function _devgen_ui_view_task_update($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -1);
  $name = end($parents);

  $path = implode("']['", $parents);
  $values = NULL;
  eval("\$values = \$form_state['values']['{$path}']['properties'];");

  $form_state['devgen']->get($name)->submitSettings($values);
  $form_state['devgen']->save();
}

/**
 * Submit callback for the remove task button.
 */
function _devgen_ui_view_task_remove($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -1);
  $name = end($parents);

  $form_state['devgen']->del($name);
  $form_state['devgen']->save();
}

/**
 * AJAX callback for adding a new property.
 */
function ajax_devgen_ui_view_add_property($form, &$form_state) {
  $commands = array();

  $name = $form_state['values']['properties']['new']['name'];
  $commands[] = ajax_command_replace('#current-tokens', drupal_render($form['tasks']['tokens']));
  $commands[] = ajax_command_append('#container-properties', drupal_render($form['properties']['container'][$name]));
  $commands[] = ajax_command_replace('#new-property', drupal_render($form['properties']['new']));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * AJAX callback for updating an existing property.
 */
function ajax_devgen_ui_view_update_property($form, &$form_state) {
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);

  $path = implode("']['", $parents);
  $element = NULL;
  eval("\$element = &\$form['{$path}'];");

  return drupal_render($element);
}

/**
 * AJAX callback for removing an existing property.
 */
function ajax_devgen_ui_view_remove_property($form, &$form_state) {
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);
  $name = end($parents);

  $path = implode("']['", $parents);
  $element = NULL;
  eval("\$element = &\$form['{$path}'];");

  $commands = array();

  $commands[] = ajax_command_replace('#current-tokens', drupal_render($form['tasks']['tokens']));
  $commands[] = $element ? ajax_command_remove("#property-container--{$name}") : ajax_command_replace("#property-container--{$name}", drupal_render($element));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * AJAX callback for adding a new task.
 */
function ajax_devgen_ui_view_add_task($form, &$form_state) {
  $commands = array();

  $name = $form_state['values']['tasks']['new']['name'];
  $commands[] = ajax_command_replace('#container-tasks-cards', drupal_render($form['tasks']['cards']));
  $commands[] = ajax_command_append('#container-tasks', drupal_render($form['tasks']['container'][$name]));
  $commands[] = ajax_command_replace('#new-task', drupal_render($form['tasks']['new']));
  $commands[] = ajax_command_replace('#current-tokens', drupal_render($form['tasks']['tokens']));
  $commands[] = ajax_command_invoke('#devgen-ui-view', 'trigger', array(
    'updateEntryPoints',
    $form_state['devgen']->getTaskOptions(),
  ));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * AJAX callback for updating an existing task.
 */
function ajax_devgen_ui_view_update_task($form, &$form_state) {
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -1);

  $path = implode("']['", $parents);
  $element = NULL;
  eval("\$element = &\$form['{$path}'];");

  return drupal_render($element);
}

/**
 * AJAX callback for removing an existing task.
 */
function ajax_devgen_ui_view_remove_task($form, &$form_state) {
  $parents = array_slice($form_state['triggering_element']['#parents'], 0, -1);
  $name = end($parents);

  $path = implode("']['", $parents);
  $element = NULL;
  eval("\$element = &\$form['{$path}'];");

  $commands = array();
  $commands[] = $element ? ajax_command_remove("#task-container--{$name}") : ajax_command_replace("#task-container--{$name}", drupal_render($element));
  $commands[] = ajax_command_replace('#current-tokens', drupal_render($form['tasks']['tokens']));
  $commands[] = ajax_command_invoke('#devgen-ui-view', 'trigger', array(
    'updateEntryPoints',
    $form_state['devgen']->getTaskOptions(),
  ));

  return array('#type' => 'ajax', '#commands' => $commands);
}
