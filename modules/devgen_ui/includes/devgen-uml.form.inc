<?php
/**
 * @file
 * A form callback to view the specified devgen.
 */

/**
 * The main devgen view form callback.
 */
function devgen_ui_uml($form, &$form_state, $name = NULL) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $devgen = devgen_new($name, $config);
  $form_state['devgen'] = $devgen;

  drupal_add_library('system', 'ui.draggable');
  $path = drupal_get_path('module', 'devgen_ui');

  $form['#attached']['css'] = array(
    "{$path}/assets/css/devgen-ui.css",
  );

  $form['#attached']['js'] = array(
    "{$path}/assets/js/devgen-ui.js",
    'https://raw.githubusercontent.com/sporritt/jsPlumb/1.7.4/dist/js/jquery.jsPlumb-1.7.4-min.js',
    "{$path}/assets/js/devgen-ui-jsplumb.js",
  );

  $form['#tree'] = TRUE;

  $form['tasks'] = array(
    '#prefix' => '<div id="container-tasks-cards">',
    '#suffix' => '</div>',
  );

  $form['tasks']['sheet'] = array(
    '#prefix' => '<div id="container-tasks-cards-sheet">',
    '#suffix' => '</div>',
  );

  $form['tasks']['container'] = array(
    '#prefix' => '<div id="container-tasks">',
    '#suffix' => '</div>',
  );

  $form['tasks']['sheet']['--entry_point'] = array(
    '#prefix' => '<div ' . drupal_attributes(array(
      'id' => 'task-card----entry_point',
      'class' => array('task-card', 'task-card-core'),
      'style' => array(
        'top: ' . round($form_state['devgen']->getSetting('entry_point_top', 0)) . 'px;',
        'left: ' . round($form_state['devgen']->getSetting('entry_point_left', 0)) . 'px;',
      ),
      'attr-connector-out' => json_encode(array(
        $form_state['devgen']->getSetting('entry_point') => '',
      )),
      'attr-task' => '--entry_point',
      'attr-has-in' => FALSE,
      'attr-has-out' => TRUE,
    )) . '>',
    '#suffix' => '</div>',
  );

  $form['tasks']['sheet']['--entry_point']['name'] = array(
    '#prefix' => '<span class="name">',
    '#suffix' => '</span>',
    '#markup' => t('Entry point'),
  );

  $form['tasks']['sheet']['--entry_point']['top'] = array(
    '#type' => 'hidden',
    '#default_value' => $form_state['devgen']->getSetting('entry_point_top', 0),
    '#attributes' => array(
      'attr-target' => 'top',
    ),
  );

  $form['tasks']['sheet']['--entry_point']['left'] = array(
    '#type' => 'hidden',
    '#default_value' => $form_state['devgen']->getSetting('entry_point_left', 0),
    '#attributes' => array(
      'attr-target' => 'left',
    ),
  );

  $tasks = $form_state['devgen']->getTaskNames();
  foreach ($tasks as $name) {
    $task = $form_state['devgen']->get($name);
    $task_type = $task instanceof DevGenAction ? 'action' : 'condition';

    $connectors = array();
    foreach ($task->exitPoints() as $id => $exit_point) {
      $ep = $task->getSetting($id);
      if (!$ep) {
        continue;
      }

      $connectors[$ep] = $exit_point['title'];
    }

    $attributes = drupal_attributes(array(
      'id' => "task-card--{$name}",
      'class' => array('task-card', "task-card-{$task_type}"),
      'style' => array(
        'top: ' . round($task->getSetting('top', 0)) . 'px;',
        'left: ' . round($task->getSetting('left', 0)) . 'px;',
      ),
      'attr-task' => $name,
      'attr-connector-out' => json_encode($connectors),
    ));

    $form['tasks']['sheet'][$name] = array(
      '#prefix' => "<div {$attributes}>",
      '#suffix' => '</div>',
    );

    $form['tasks']['sheet'][$name]['name'] = array(
      '#prefix' => '<span class="name">',
      '#suffix' => '</span>',
      '#markup' => $name,
    );

    $form['tasks']['sheet'][$name]['top'] = array(
      '#type' => 'hidden',
      '#default_value' => $task->getSetting('top', 0),
      '#attributes' => array(
        'attr-target' => 'top',
      ),
    );

    $form['tasks']['sheet'][$name]['left'] = array(
      '#type' => 'hidden',
      '#default_value' => $task->getSetting('left', 0),
      '#attributes' => array(
        'attr-target' => 'left',
      ),
    );
  }

  $form['actions'] = array('#type' => 'actions', '#weight' => 1);
  $form['actions']['update_positions'] = array(
    '#type' => 'submit',
    '#value' => t('Update positions'),
    '#ajax' => array(
      'wrapper' => 'container-tasks-cards-sheet',
      'callback' => 'ajax_devgen_ui_uml_update_positions',
    ),
    '#submit' => array('_devgen_ui_uml_tasks_positions_update'),
    '#limit_validation_errors' => array(
      array('tasks'),
    ),
  );

  return $form;
}

/**
 * Submit callback for the update positions button.
 */
function _devgen_ui_uml_tasks_positions_update($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $positions = $form_state['values']['tasks']['sheet'];

  foreach ($positions as $task_id => $coords) {
    if (strpos($task_id, '--') === 0) {
      $core_id = substr($task_id, 2);
      foreach ($coords as $coord => $value) {
        $form_state['devgen']->setSetting("{$core_id}_{$coord}", $value);
      }
    }
    else {
      $task = $form_state['devgen']->get($task_id);
      if ($task) {
        foreach ($coords as $coord => $value) {
          $task->setSetting($coord, $value);
        }
      }
    }
  }

  $form_state['devgen']->save();
}

/**
 * AJAX callback for update uml positions.
 */
function ajax_devgen_ui_uml_update_positions($form, &$form_state) {
  return drupal_render($form['tasks']['sheet']);
}
