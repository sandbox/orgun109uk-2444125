<?php
/**
 * @file
 * A form callback to delete the specified devgen.
 */

/**
 * Delete form callback.
 */
function devgen_ui_delete($form, &$form_state, $name = NULL) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $devgen = devgen_new($name, $config);
  $form_state['devgen'] = $devgen;

  $form['_title'] = array(
    '#type' => 'item',
    '#title' => t('Title'),
    '#markup' => $devgen->getTitle(),
  );

  $form['_description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#markup' => $devgen->getDescription(),
  );

  return confirm_form(
    $form,
    'Are you sure you want to delete this devgen?',
    DEVGEN_UI_MENU,
    NULL,
    'Delete'
  );
}

/**
 * Submit callback for the delete form.
 */
function devgen_ui_delete_submit($form, &$form_state) {
  $form_state['redirect'] = DEVGEN_UI_MENU;

  if (devgen_delete($form_state['devgen']->getName())) {
    drupal_set_message(t('You have successfully deleted the devgen @name.', array(
      '@name' => $form_state['devgen']->getName(),
    )));
  }
  else {
    drupal_set_message(t('Failed to delete the devgen @name.', array(
      '@name' => $form_state['devgen']->getName(),
    )), 'warning');
  }
}
