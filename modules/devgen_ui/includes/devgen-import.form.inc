<?php
/**
 * @file
 * Provides the import form.
 */

/**
 * The import form callback.
 */
function devgen_ui_import($form, &$form_state) {
  $form = array();

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#description' => t('Override the name from the config, leave blank to use the name from the config.'),
    '#maxlength' => 21,
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'devgen_name_exists',
    ),
  );

  $form['import'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#rows' => 15,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Validate callback for the import form.
 */
function devgen_ui_import_validate($form, &$form_state) {
  try {
    $config = devgen_import_validate(
      $form_state['values']['import'],
      isset($form_state['values']['name']) ? $form_state['values']['name'] : NULL
    );
    $form_state['devgen'] = $config;
  }
  catch (Exception $err) {
    form_set_error('import', $err->getMessage());
  }
}

/**
 * Submit callback for the import form.
 */
function devgen_ui_import_submit($form, &$form_state) {
  if (devgen_save($form_state['devgen']['name'], $form_state['devgen'])) {
    drupal_set_message(t('Successfully imported devgen @name', array(
      '@name' => $form_state['devgen']['name'],
    )));

    $form_state['redirect'] = DEVGEN_UI_MENU . "/devgen/{$form_state['devgen']['name']}";
  }
  else {
    drupal_set_message(t('Failed to import the devgen config.'), 'warning');
  }
}
