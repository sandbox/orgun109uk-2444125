<?php
/**
 * @file
 * List the available devgens.
 */

/**
 * DevGens list form callback.
 */
function devgen_ui_list($form, &$form_state) {
  $form = array();

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'devgen_ui') . '/assets/css/devgen-ui.css',
  );

  $header = array(
    array('data' => t('Title'), 'width' => 250),
    array('data' => t('Description')),
    array('data' => t('Created'), 'width' => 250),
    array('data' => t('Modified'), 'width' => 250),
    array('data' => t('Storage'), 'width' => 100),
    array('data' => t('Operations'), 'width' => 150),
  );

  $rows = array();

  $devgens = devgen_devgen_configs();
  if ($devgens && count($devgens)) {
    $ctools = module_exists('ctools');
    if ($ctools) {
      ctools_include('dropbutton.theme');
    }

    foreach ($devgens as $name => $devgen) {
      $links = menu_contextual_links('devgen', DEVGEN_UI_MENU . '/devgen', array($name));
      if ($ctools) {
        $operations = theme('links__ctools_dropbutton', array(
          'title' => t('operations'),
          'links' => $links,
        ));
      }
      else {
        $operations = theme('links', array(
          'links' => $links,
        ));
      }

      if (isset($devgen['_storage']) && $devgen['_storage'] == 'overriden') {
        $storage = t('Overriden');
      }
      elseif (isset($devgen['_storage']) && $devgen['_storage'] == 'database') {
        $storage = t('Database');
      }
      else {
        $storage = t('Code');
      }

      $rows[] = array(
        "{$devgen['title']} [{$name}]",
        $devgen['description'],
        !empty($devgen['createdOn']) ? t('@time by @user', array(
          '@time' => format_date($devgen['createdOn']),
          '@user' => $devgen['createdBy'],
        )) : '-',
        !empty($devgen['modifiedOn']) ? t('@time by @user', array(
          '@time' => format_date($devgen['modifiedOn']),
          '@user' => $devgen['modifiedBy'],
        )) : '-',
        $storage,
        array(
          'data' => $operations,
          'class' => array('row-operations'),
        ),
      );
    }
  }

  $form['devgens'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are currently no devgens available, !add one.', array(
      '!add' => l(t('add'), DEVGEN_UI_MENU . '/add'),
    )),
  );

  return $form;
}
