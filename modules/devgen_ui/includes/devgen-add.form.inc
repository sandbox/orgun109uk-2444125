<?php
/**
 * @file
 * A form callback to add the specified devgen.
 */

/**
 * Form callback for the add form.
 */
function devgen_ui_add($form, &$form_state) {
  $form = array();

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#description' => t('The name to give to the new devgen.'),
    '#maxlength' => 21,
    '#machine_name' => array(
      'exists' => 'devgen_name_exists',
    ),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of the new devgen.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The description of the new devgen.'),
  );

  $form['actions'] = array('#type' => 'action');
  $form['actions']['create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('cancel'), DEVGEN_UI_MENU),
  );

  return $form;
}

/**
 * The validate callback for the add form.
 */
function devgen_ui_add_validate($form, &$form_state) {
  $devgen = devgen_config($form_state['values']['name']);
  if ($devgen) {
    form_set_error($form['name'], t('A devgen already exists with this name.'));
  }
}

/**
 * The submit callback for the add form.
 */
function devgen_ui_add_submit($form, &$form_state) {
  $config = array(
    'title' => $form_state['values']['title'],
    'description' => $form_state['values']['description'],
  );

  $devgen = devgen_new($form_state['values']['name'], $config);
  if ($devgen->save()) {
    drupal_set_message(t('DevGen has successfully been created.'));
    $form_state['redirect'] = DEVGEN_UI_MENU . "/devgen/{$form_state['values']['name']}";
  }
  else {
    drupal_set_message(t('There was a problem saving the devgen.'), 'error');
  }
}
