<?php
/**
 * @file
 * A form callback to run or evaluate the specified devgen.
 */

/**
 * Run form callback.
 */
function devgen_ui_run($form, &$form_state, $name = NULL, $real_run = FALSE) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'devgen_ui') . '/assets/css/devgen-ui.css',
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'devgen_ui') . '/assets/js/devgen-ui.js',
  );

  $devgen = devgen_new($name, $config);
  $devgen->execute($real_run == FALSE);

  $form['output'] = array(
    '#theme' => 'devgen_output',
    '#devgen' => $devgen,
  );

  return $form;
}
