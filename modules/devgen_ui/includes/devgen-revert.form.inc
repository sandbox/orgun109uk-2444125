<?php
/**
 * @file
 * A form callback to revert the specified devgen.
 */

/**
 * Revert form callback.
 */
function devgen_ui_revert($form, &$form_state, $name = NULL) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $devgen = devgen_new($name, $config);
  $form_state['devgen'] = $devgen;

  $form['_title'] = array(
    '#type' => 'item',
    '#title' => t('Title'),
    '#markup' => $devgen->getTitle(),
  );

  $form['_description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#markup' => $devgen->getDescription(),
  );

  return confirm_form(
    $form,
    'Are you sure you want to revert this devgen?',
    DEVGEN_UI_MENU,
    NULL,
    'Revert'
  );
}

/**
 * Submit callback for the revert form.
 */
function devgen_ui_revert_submit($form, &$form_state) {
  $form_state['redirect'] = DEVGEN_UI_MENU . '/' . $form_state['devgen']->getName() . '/view';

  if (devgen_delete($form_state['devgen']->getName())) {
    drupal_set_message(t('You have successfully reverted the devgen @name.', array(
      '@name' => $form_state['devgen']->getName(),
    )));
  }
  else {
    drupal_set_message(t('Failed to revert the devgen @name.', array(
      '@name' => $form_state['devgen']->getName(),
    )), 'warning');
  }
}
