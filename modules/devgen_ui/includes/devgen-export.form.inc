<?php
/**
 * @file
 * A form callback to view the specified devgen.
 */

/**
 * Helper function to output an array to markup.
 *
 * @param array $data
 *   The array data to markup.
 * @param array $markup
 *   A reference to the markup output.
 * @param int $indent
 *   (optional = 6) The size of the indent.
 */
function _devgen_ui_array_to_markup(array $data, array &$markup, $indent = 6) {
  foreach ($data as $key => $value) {
    if (is_array($value)) {
      if (count($value) === 0) {
        $markup[] = str_pad('', $indent, ' ') . "'{$key}' => array(),";
      }
      else {
        $markup[] = str_pad('', $indent, ' ') . "'{$key}' => array(";
        _devgen_ui_array_to_markup($value, $markup, $indent + 2);
        $markup[] = str_pad('', $indent, ' ') . '),';
      }
    }
    else {
      if (is_null($value)) {
        $v = 'NULL';
      }
      elseif (is_bool($value)) {
        $v = $value ? 'TRUE' : 'FALSE';
      }
      elseif (is_numeric($value)) {
        $v = $value;
      }
      else {
        $v = "'{$value}'";
      }
      $markup[] = str_pad('', $indent, ' ') . "'{$key}' => {$v},";
    }
  }
}

/**
 * The export form callback.
 */
function devgen_ui_export($form, &$form_state, $name = NULL) {
  $form = array();

  $config = devgen_config($name);
  if (!$config) {
    drupal_set_message(t('Unable to find a matching devgen config.'), 'error');
    return drupal_goto(DEVGEN_UI_MENU);
  }

  $devgen = devgen_new($name, $config);

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'edit_json',
  );

  $form['json'] = array(
    '#type' => 'fieldset',
    '#title' => t('JSON'),
    '#group' => 'tabs',
  );

  $form['json']['export'] = array(
    '#prefix' => '<pre style="border: 1px solid #ccc; width: 100%; height: 450px; padding: 10px; overflow: auto;">',
    '#suffix' => '</pre>',
    '#markup' => $devgen->__toString(),
  );

  $form['php'] = array(
    '#type' => 'fieldset',
    '#title' => t('PHP'),
    '#group' => 'tabs',
  );

  $export = $devgen->export();
  unset(
    $export['name'],
    $export['createdOn'], $export['createdBy'],
    $export['modifiedOn'], $export['modifiedBy']
  );

  $markup = array();
  $markup[] = '/**';
  $markup[] = ' * Implements hook_devgens().';
  $markup[] = ' */';
  $markup[] = 'function MYMODULE_devgens() {';
  $markup[] = '  return array(';
  $markup[] = "    '{$devgen->getName()}' => array(";
  _devgen_ui_array_to_markup($export, $markup);
  $markup[] = '    ),';
  $markup[] = '  );';
  $markup[] = '}';
  $markup[] = '';

  $form['php']['export'] = array(
    '#prefix' => '<pre style="border: 1px solid #ccc; width: 100%; height: 450px; padding: 10px; overflow: auto;">',
    '#suffix' => '</pre>',
    '#markup' => implode("\n", $markup),
  );

  return $form;
}
