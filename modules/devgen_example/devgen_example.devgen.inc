<?php
/**
 * @file
 * Define the example devgen hooks.
 */

/**
 * Implements hook_devgens().
 */
function devgen_example_devgens() {
  return array(
    'devgen_example' => array(
      'title' => 'Example',
      'description' => 'An example DevGen configuration.',
      'settings' => array(
        'entry_point' => 'verify_user',
        'loops' => 10,
      ),
      'properties' => array(
        'username' => array(
          'name' => 'randomString',
          'value' => NULL,
          'config' => array(),
        ),
      ),
      'tasks' => array(
        'verify_user' => array(
          'name' => 'verifyUser',
          'config' => array(
            'negate' => 0,
            'field' => 'name',
            'operation' => '=',
            'value' => '@{username}',
            'success_exit_point' => 'skipped_user_message',
            'fail_exit_point' => 'create_user',
          ),
        ),
        'create_user' => array(
          'name' => 'createUser',
          'config' => array(
            'name' => '@{username}',
            'mail' => '@{username}@devgen.dev',
            'password' => 'password',
            'roles' => array(
              'authenticated user' => 'authenticated user',
            ),
            'success_exit_point' => 'created_user_message',
            'fail_exit_point' => '',
          ),
        ),
        'created_user_message' => array(
          'name' => 'drupalSetMessage',
          'config' => array(
            'message' => 'Successfully created the user @{create_user:name}',
            'success_exit_point' => '',
            'fail_exit_point' => '',
          ),
        ),
        'skipped_user_message' => array(
          'name' => 'drupalSetMessage',
          'config' => array(
            'message' => 'The user @{create_user:name} already exists.',
            'success_exit_point' => '',
            'fail_exit_point' => '',
          ),
        ),
      ),
    ),
  );
}
