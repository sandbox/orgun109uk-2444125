<?php
/**
 * @file
 * Provides the create commerce order action.
 */

/**
 * Create commerce order action class.
 */
class DevGenActionCommerceOrderCreate extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onResetProperties() {
    $properties = parent::onResetProperties();
    return array_merge($properties, array(
      'order_id' => 'Order ID',
      'order' => 'Commerce order object',
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $user = $this->getSetting('user', NULL, TRUE);
    $status = $this->getSetting('status', NULL, TRUE);
    $line_items = $this->getSetting('line_items', array(), FALSE);

    if (!$user || !isset($user->uid)) {
      throw new Exception(t('Undefined user.'));
    }

    if (empty($line_items)) {
      throw new Exception(t('Undefined line items.'));
    }

    $success = $evaluate ? TRUE : FALSE;

    $this->devgen->message('Creating commerce order...');
    if (!$evaluate) {
      $order = commerce_order_new($user->uid, $status, 'commerce_order');
      commerce_order_save($order);

      $success = $order && isset($order->order_id);
      if ($success) {
        $wrapper = entity_metadata_wrapper('commerce_order', $order);
        foreach ($line_items as $val) {
          $sku = $this->devgen->parseTokens('@{' . $val . ':sku}');
          $qty = (int) $this->devgen->parseTokens('@{' . $val . ':quantity}');

          $product = commerce_product_load_by_sku($sku);
          if ($product) {
            $line_item = commerce_product_line_item_new($product, max(1, (int) $qty), $order->order_id);
            commerce_line_item_save($line_item);

            $wrapper->commerce_line_items[] = $line_item;
          }
        }

        commerce_order_save($order);

        $this->setProperty('order_id', $order->order_id);
        $this->setProperty('order', $order);
      }
    }
    else {
      $lis = array();
      foreach ($line_items as $val) {
        $sku = $this->devgen->parseTokens('@{' . $val . ':sku}');
        $qty = (int) $this->devgen->parseTokens('@{' . $val . ':quantity}');

        $product = commerce_product_load_by_sku($sku);
        $lis[] = (object) array(
          'line_item_id' => 0,
          'order_id' => 0,
          'product_id' => $product->product_id,
          'sku' => $sku,
          'qty' => $qty,
        );
      }

      $this->setProperty('order_id', 0);
      $this->setProperty('order', (object) array(
        'order_id' => 0,
        'line_items' => $lis,
      ));
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettings(array $settings) {
    $line_items = array();
    if (is_array($settings['line_items']) && count($settings['line_items'])) {
      foreach ($settings['line_items'] as $line_item) {
        if (empty($line_item)) {
          continue;
        }

        $line_items[] = $line_item;
      }
    }

    $settings['line_items'] = $line_items;
    parent::submitSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['user'] = array(
      '#type' => 'textfield',
      '#title' => t('User'),
      '#default_value' => $values ? $values['user'] : $this->getSetting('user'),
      '#required' => TRUE,
    );

    $form['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => commerce_order_status_options_list(),
      '#default_value' => $values ? $values['status'] : $this->getSetting('status'),
      '#required' => TRUE,
    );

    $form['line_items'] = array(
      '#type' => 'fieldset',
      '#title' => t('Line items'),
      '#prefix' => '<div id="commerce-order-line-items--' . $this->name . '">',
      '#suffix' => '</div>',
      '#collapsible' => TRUE,
      '#collapsed' => $values === NULL,
    );

    $line_items = $values ? $values['line_items'] : $this->getSetting('line_items', array());
    $options = $this->devgen->getTaskOptions('action', 'commerceLineItemCreate');
    if (is_array($line_items) && count($line_items)) {
      foreach ($line_items as $line_item) {
        if (empty($line_item)) {
          continue;
        }

        $form['line_items'][] = array(
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => $line_item,
          '#ajax' => array(
            'wrapper' => "commerce-order-line-items--{$this->name}",
            'callback' => 'ajax_devgen_ui_view_update_task',
          ),
        );
      }
    }

    $form['line_items'][] = array(
      '#type' => 'select',
      '#options' => $options,
      '#ajax' => array(
        'wrapper' => "commerce-order-line-items--{$this->name}",
        'callback' => 'ajax_devgen_ui_view_update_task',
      ),
    );

    return $form;
  }

}
