<?php
/**
 * @file
 * Provides the create commerce line item action.
 */

/**
 * Create commerce line item action class.
 */
class DevGenActionCommerceLineItemCreate extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onResetProperties() {
    $properties = parent::onResetProperties();
    return array_merge($properties, array(
      'quantity' => 'Quantity.',
      'sku' => 'The line item SKU.',
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $this->setProperty('quantity', $this->getSetting('quantity'));
    $this->setProperty('sku', $this->getSetting('sku'));

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $type = $values ? $values['type'] : $this->getSetting('type');

    $form['#prefix'] = '<div id="line-item-settings--' . $this->name . '">';
    $form['#suffix'] = '</div>';

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => commerce_line_item_type_options_list(),
      '#default_value' => $type,
      '#required' => TRUE,
      '#ajax' => array(
        'wrapper' => "line-item-settings--{$this->name}",
        'callback' => 'ajax_devgen_ui_view_update_task',
      ),
    );

    if (!empty($type)) {
      if ($type === 'product') {
        $form['sku'] = array(
          '#type' => 'textfield',
          '#title' => t('SKU'),
          '#default_value' => $values ? $values['sku'] : $this->getSetting('sku'),
          '#required' => TRUE,
        );

        $form['quantity'] = array(
          '#type' => 'textfield',
          '#title' => t('Quantity'),
          '#default_value' => $values ? $values['quantity'] : $this->getSetting('quantity'),
          '#required' => TRUE,
        );
      }
    }

    return $form;
  }

}
