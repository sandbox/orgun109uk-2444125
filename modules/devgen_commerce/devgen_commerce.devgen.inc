<?php
/**
 * @file
 * Provides devgen commerce tasks, conditions and properties.
 */

/**
 * Implements hook_devgen_tasks().
 */
function devgen_commerce_devgen_tasks() {
  return array(
    // DevGen actions.
    'commerceOrderCreate' => array(
      'title' => t('Commerce Order - Create'),
      'description' => t('Creates a commerce order.'),
      'classname' => 'DevGenActionCommerceOrderCreate',
    ),
    'commerceLineItemCreate' => array(
      'title' => t('Commerce Line Item - Create'),
      'description' => t('Creates a commerce line item.'),
      'classname' => 'DevGenActionCommerceLineItemCreate',
    ),
  );
}
