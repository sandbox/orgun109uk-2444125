<?php
/**
 * @file
 * Provides devgen subscriptions tasks, conditions and properties.
 */

/**
 * Implements hook_devgen_tasks().
 */
function devgen_subscriptions_devgen_tasks() {
  return array(
    // DevGen actions.
    'subscriptionsOrderCreate' => array(
      'title' => t('Subscriptions Order - Create'),
      'description' => t('Creates a subscriptions order.'),
      'classname' => 'DevGenActionSubscriptionsOrderCreate',
    ),
    'subscriptionsOrderProcessNextItem' => array(
      'title' => t('Subscriptions Order - Process next item'),
      'description' => t('Processes the next available scheduled item of the subscription order.'),
      'classname' => 'DevGenActionSubscriptionsOrderProcessNextItem',
    ),

    // DevGen conditions.
    'subscriptionIsSubscriptionsOrder' => array(
      'title' => t('Subscriptions Order - Is subscriptions order'),
      'description' => t('Passes if the order is a subscriptions order.'),
      'classname' => 'DevGenConditionSubscriptionIsSubscriptionOrder',
    ),
    'subscriptionCompleted' => array(
      'title' => t('Subscriptions Order - Has been completed'),
      'description' => t('Passes if the subscriptions order has been completed, or no other pending payments.'),
      'classname' => 'DevGenConditionSubscriptionCompleted',
    ),
  );
}
