<?php
/**
 * @file
 * Provides the create subscriptions order action.
 */

/**
 * Create subscriptions order action class.
 */
class DevGenActionSubscriptionsOrderCreate extends DevGenActionCommerceOrderCreate {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $user = $this->getSetting('user', NULL, TRUE);
    $status = $this->getSetting('status', NULL, TRUE);
    $line_items = $this->getSetting('line_items', array(), FALSE);

    if (!$user || !isset($user->uid)) {
      throw new Exception(t('Undefined user.'));
    }

    if (empty($line_items)) {
      throw new Exception(t('Undefined line items.'));
    }

    $success = $evaluate ? TRUE : FALSE;

    $this->devgen->message('Creating subscription order...');
    if (!$evaluate) {
      $order = commerce_order_new($user->uid, $status, 'commerce_order');
      commerce_order_save($order);

      $success = $order && isset($order->order_id);
      if ($success) {
        $wrapper = entity_metadata_wrapper('commerce_order', $order);
        foreach ($line_items as $val) {
          $sku = $this->devgen->parseTokens('@{' . $val . ':sku}');
          $qty = (int) $this->devgen->parseTokens('@{' . $val . ':quantity}');

          $product = commerce_product_load_by_sku($sku);
          if ($product) {
            $line_item = commerce_product_line_item_new($product, max(1, (int) $qty), $order->order_id);
            commerce_line_item_save($line_item);

            $wrapper->commerce_line_items[] = $line_item;
          }
        }

        commerce_order_save($order);

        subscriptions_create_prepaid_plan($order, 'ORDER_CODE', 0, 0);

        $this->setProperty('order_id', $order->order_id);
        $this->setProperty('order', $order);
      }
    }
    else {
      $lis = array();
      foreach ($line_items as $val) {
        $sku = $this->devgen->parseTokens('@{' . $val . ':sku}');
        $qty = (int) $this->devgen->parseTokens('@{' . $val . ':quantity}');

        $product = commerce_product_load_by_sku($sku);
        $lis[] = (object) array(
          'line_item_id' => 0,
          'order_id' => 0,
          'product_id' => $product->product_id,
          'sku' => $sku,
          'qty' => $qty,
        );
      }

      $this->setProperty('order_id', 0);
      $this->setProperty('order', (object) array(
        'order_id' => 0,
        'line_items' => $lis,
      ));
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $options = array();
    foreach (subscriptions_get_all_payment_plans() as $payment_plan) {
      $options[$payment_plan->name] = $payment_plan->label;
    }

    $form['payment_plan'] = array(
      '#type' => 'select',
      '#title' => t('Payment plan'),
      '#options' => $options,
      '#default_value' => $this->getSetting('payment_plan'),
      '#required' => TRUE,
    );

    return $form;
  }

}
