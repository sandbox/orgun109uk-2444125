<?php
/**
 * @file
 * Provides the create subscriptions order process next item action.
 */

/**
 * Create subscriptions order process next item action class.
 */
class DevGenActionSubscriptionsOrderProcessNextItem extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $order = $this->getSetting('order', NULL, TRUE);
    if (!$order || !isset($order->order_id)) {
      throw new Exception(t('Undefined order.'));
    }

    $scheduled_item_group = subscriptions_get_scheduled_items_for_order($order->order_id);
    if (!$scheduled_item_group) {
      throw new Exception(t('Either not a subscription order or unable to the orders schedule.'));
    }

    $next_scheduled_item = $scheduled_item_group->getNextItemToProcess();
    if (!$next_scheduled_item) {
      throw new Exception(t('Unable to get the next schedule item.'));
    }

    $this->devgen->message('Processing the next schedule item...');
    $success = FALSE;
    if (!$evaluate) {
      // @todo
    }
    else {
      // @todo
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['payment_plan'] = array(
      '#type' => 'textfield',
      '#title' => t('Subscription order'),
      '#default_value' => $this->getSetting('order'),
      '#required' => TRUE,
    );

    return $form;
  }

}
