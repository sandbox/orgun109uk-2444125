<?php
/**
 * @file
 * Provides the subscription completed condition.
 */

/**
 * Create subscription completed condition class.
 */
class DevGenConditionSubscriptionCompleted extends DevGenCondition {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $order = $this->getSetting('order', NULL, TRUE);

    if (!$order || !isset($order->order_id)) {
      throw new Exception(t('Undefined order.'));
    }

    $scheduled_item_group = subscriptions_get_scheduled_items_for_order($order->order_id);

    $success = $scheduled_item_group->getNextItemToProcess() === NULL;
    if ($this->getSetting('negate', FALSE)) {
      $success = !$success;
    }

    $this->devgen->message(t('Checking if subscription order @id has been completed.', array(
      '@id' => $order->order_id,
    )));

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['order'] = array(
      '#type' => 'textfield',
      '#title' => t('Subscription order'),
      '#default_value' => $this->getSetting('order'),
      '#required' => TRUE,
    );

    return $form;
  }

}
