<?php
/**
 * @file
 * Provides the subscription is subscription condition.
 */

/**
 * Create subscription is subscription condition class.
 */
class DevGenConditionSubscriptionIsSubscriptionOrder extends DevGenCondition {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $order = $this->getSetting('order', NULL, TRUE);

    if (!$order || !isset($order->order_id)) {
      throw new Exception(t('Undefined order.'));
    }

    $success = subscriptions_is_subscription_order($order);
    if ($this->getSetting('negate', FALSE)) {
      $success = !$success;
    }

    $this->devgen->message(t('Checking if order @id is a subscription order.', array(
      '@id' => $order->order_id,
    )));

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['order'] = array(
      '#type' => 'textfield',
      '#title' => t('Commerce order'),
      '#default_value' => $values ? $values['user'] : $this->getSetting('order'),
      '#required' => TRUE,
    );

    return $form;
  }

}
