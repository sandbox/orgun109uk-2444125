<?php
/**
 * @file
 * Form alter group file.
 */

/**
 * Implements hook_form_FORMID_alter().
 */
function devgen_rollback_form_devgen_ui_view_alter(&$form, &$form_state) {
  $form_state['build_info']['files']['devgen_rollback'] = drupal_get_path('module', 'devgen_rollback') . '/devgen_rollback.form-alter.inc';

  $form['rollback'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rollback'),
    '#group' => 'tabs',
  );

  $rollbacks = $form_state['devgen']->getRollback()->getRollbacks();
  if (empty($rollbacks)) {
    $form['rollback']['rollbacks'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('This config currently has not rollbacks assigned.'),
    );
  }
  else {
    $form['rollback']['rollbacks'] = array('#tree' => TRUE);
    foreach ($rollbacks as $run_id => $run_rollbacks) {
      $form['rollback']['rollbacks'][$run_id] = array(
        '#type' => 'fieldset',
        '#title' => t('Run ID #@runId', array('@runId' => $run_id)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form['rollback']['rollbacks'][$run_id]['table'] = array(
        '#theme' => 'table',
        '#header' => array(
          array('data' => t('Type'), 'width' => 100),
          array('data' => t('Created On'), 'width' => 250),
          t('Details'),
        ),
        '#rows' => array(),
      );

      $form['rollback']['rollbacks'][$run_id]['rollback'] = array(
        '#type' => 'submit',
        '#name' => "rollback-{$run_id}",
        '#value' => t('Rollback'),
        '#submit' => array('devgen_rollback_rollback_submit'),
        '#attributes' => array(
          'onclick' => 'return confirm("' . t('Are you sure you want to rollback this run?') . '");',
        ),
      );

      foreach ($run_rollbacks as $idx => $rollback) {
        $form['rollback']['rollbacks'][$run_id]['table']['#rows'][] = array(
          $rollback['type'],
          format_date($rollback['createdOn']),
          $rollback['info'],
        );
      }
    }

    $form['rollback']['actions'] = array('#type' => 'actions');
    $form['rollback']['actions']['rollback_all'] = array(
      '#type' => 'submit',
      '#value' => t('Rollback all'),
      '#submit' => array('devgen_rollback_rollback_all_submit'),
      '#attributes' => array(
        'onclick' => 'return confirm("' . t('Are you sure you want to rollback everything?') . '");',
      ),
    );
  }
}

/**
 * Submit callback for the 'rollback' button for each group.
 */
function devgen_rollback_rollback_submit($form, &$form_state) {
  $parents = $form_state['triggering_element']['#parents'];
  array_pop($parents);

  $form_state['devgen']->getRollback()->rollback(array_pop($parents));
}

/**
 * Submit callback for the 'rollback all' button.
 */
function devgen_rollback_rollback_all_submit($form, &$form_state) {
  $form_state['devgen']->getRollback()->rollback();
}
