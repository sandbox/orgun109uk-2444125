<?php
/**
 * @file
 * Defines the devgen rollback class.
 */

/**
 * Class DevGenRollback.
 */
class DevGenRollback {

  /**
   * The owning devgen config
   *
   * @var DevGen
   */
  protected $owner;

  /**
   * An array of rollbacks available to perform.
   *
   * @var array
   */
  protected $rollbacks;

  /**
   * The devgen output class constructor.
   *
   * @param DevGen $owner
   *   The owning devgen config class.
   */
  public function __construct(DevGen $owner) {
    $this->owner = $owner;
    $this->rollbacks = array();
    $this->restore();
  }

  /**
   * Gets the owner devgen class.
   *
   * @return DevGen
   *   The owner devgen class.
   */
  final public function getOwner() {
    return $this->owner;
  }

  /**
   * Restores the rollback data from the database.
   */
  final protected function restore() {
    $name = $this->owner->getName();
    if (!empty($name)) {
      $this->rollbacks = array();

      $dbq = db_select('devgens_rollbacks', 'dr', array('fetch' => PDO::FETCH_ASSOC));
      $dbq
        ->fields('dr')
        ->condition('dr.devgen', $name)
        ->orderBy('dr.run_id', 'ASC');

      $rollbacks = $dbq->execute()->fetchAll();
      foreach ($rollbacks as $rollback) {
        if (!isset($this->rollbacks[$rollback['run_id']])) {
          $this->rollbacks[$rollback['run_id']] = array();
        }

        $this->rollbacks[$rollback['run_id']][] = array(
          'createdOn' => $rollback['createdOn'],
          'type' => $rollback['type'],
          'info' => $rollback['info'],
          'details' => is_string($rollback['details']) ? unserialize($rollback['details']) : $rollback['details'],
        );
      }
    }
  }

  /**
   * Clear the rollbacks from this aswell as optionally the database.
   *
   * @param int $runId
   *   (Optional = NULL) The run ID to clear.
   * @param bool $fromDatabase
   *   (Optional = TRUE) Clear from the database too.
   *
   * @return DevGenRollback
   *   Returns self.
   */
  final public function clear($runId = NULL, $fromDatabase = TRUE) {
    if (empty($runId)) {
      foreach (array_keys($this->rollbacks) as $runId) {
        $this->clear($runId, $fromDatabase);
      }
    }
    else {
      unset($this->rollbacks[$runId]);

      if ($fromDatabase) {
        $dbq = db_delete('devgens_rollbacks');
        $dbq
          ->condition('devgen', $this->owner->getName())
          ->condition('run_id', $runId)
          ->execute();
      }
    }

    return $this;
  }

  /**
   * Add a single rollback record.
   *
   * @param string $type
   *   The rollback type.
   * @param string $info
   *   Some information for display purposes.
   * @param array $details
   *   The rollback details.
   * @param bool $saveToDatabase
   *   (Optional = TRUE) Set to FALSE to prevent saving to the database.
   *
   * @return DevGenRollback
   *   Returns self.
   */
  final public function add($type, $info, array $details, $saveToDatabase = TRUE) {
    $runId = $this->owner->getRunId();
    if (!isset($this->rollbacks[$runId])) {
      $this->rollbacks[$runId] = array();
    }

    $rollback = array(
      'createdOn' => time(),
      'type' => $type,
      'info' => $info,
      'details' => $details,
    );

    $this->rollbacks[$runId][] = $rollback;

    if ($saveToDatabase) {
      $record = array(
        'devgen' => $this->owner->getName(),
        'run_id' => $runId,
        'createdOn' => $rollback['createdOn'],
        'type' => $rollback['type'],
        'info' => $rollback['info'],
        'details' => $rollback['details'],
      );

      drupal_write_record('devgens_rollbacks', $record);
    }

    return $this;
  }

  /**
   * Get an array of rollback details.
   *
   * @return array
   *   An array of all the rollbacks.
   */
  final public function getRollbacks() {
    $rollbacks = array();
    foreach ($this->rollbacks as $run_id => $run_rollbacks) {
      $rollbacks[$run_id] = array();

      foreach ($run_rollbacks as $rollback) {
        $rollbacks[$run_id][] = array(
          'type' => $rollback['type'],
          'createdOn' => $rollback['createdOn'],
          'info' => $rollback['info'],
        );
      }
    }

    return $rollbacks;
  }

  /**
   * Perform the rollbacks.
   *
   * @param int $runId
   *   (Optional = NULL) The run ID to rollback, otherwise all will be rolled
   *   back.
   * @param bool $clear
   *   (Optional = TRUE) Set to FALSE to prevent clearing the rollbacks after
   *   performing the rollbacks.
   *
   * @return DevGenRollback
   *   Returns self.
   */
  final public function rollback($runId = NULL, $clear = TRUE) {
    if (empty($runId)) {
      foreach (array_keys($this->rollbacks) as $runId) {
        $this->rollback($runId, $clear);
      }
    }
    else {
      $rollbacks = isset($this->rollbacks[$runId]) ? $this->rollbacks[$runId] : array();
      if (count($rollbacks)) {
        foreach ($rollbacks as $rollback) {
          module_invoke_all("devgen_rollback_{$rollback['type']}", $rollback['type'], $rollback['details']);
        }

        if ($clear) {
          $this->clear($runId);
        }
      }
    }

    return $this;
  }

}
