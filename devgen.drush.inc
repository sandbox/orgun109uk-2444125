<?php
/**
 * @file
 * Provides several Drush commands to manage, and execute devgens.
 */

/**
 * Implements hook_drush_command().
 */
function devgen_drush_command() {
  $commands = array();

  // List available devgen configs.
  $commands['devgen-list'] = array(
    'description' => dt('List all available devgens.'),
    'examples' => array(
      'Standard example' => 'drush devgen-list',
    ),
    'aliases' => array('ds-l'),
  );

  // Execute specified devgen.
  $commands['devgen-execute'] = array(
    'description' => dt('Execute a devgen.'),
    'arguments' => array(
      'name' => dt('The name of the devgen to execute'),
    ),
    'options' => array(
      'eval' => dt('Evaluate the devgen config'),
    ),
    'examples' => array(
      'Standard example' => 'drush devgen-execute %NAME',
      'Evaluate example' => 'drush devgen-execute %NAME --eval',
    ),
    'aliases' => array('ds-ex'),
  );

  // Import specified devgen config from a JSON file.
  $commands['devgen-import'] = array(
    'description' => dt('Import devgen config JSON file.'),
    'arguments' => array(
      'filename' => dt('The devgen config to import'),
    ),
    'options' => array(
      'name' => dt('Optionally override the devgen name'),
    ),
    'examples' => array(
      'Standard example' => 'drush devgen-import %FILENAME',
      'Output to a file' => 'drush devgen-import %FILENAME --name=new_devgen_name',
    ),
    'aliases' => array('ds-imp'),
  );

  // Export specified devgen config to JSON or JSON file.
  $commands['devgen-export'] = array(
    'description' => dt('Export the devgen config to STDOUT or a JSON file.'),
    'arguments' => array(
      'config' => dt('The devgen config to export'),
    ),
    'options' => array(
      'target' => dt('The optional target filename'),
    ),
    'examples' => array(
      'Standard example' => 'drush devgen-export %NAME',
      'Output to a file' => 'drush devgen-export %NAME --target=./filename.json',
    ),
    'aliases' => array('ds-exp'),
  );

  return $commands;
}

/**
 * Callback function for drush devgen-list.
 */
function drush_devgen_list() {
  $configs = devgen_devgen_configs(TRUE);
  $rows = array(
    array(
      dt('Name'),
      dt('Title'),
      dt('Description'),
    ),
  );

  foreach ($configs as $name => $config) {
    $rows[] = array(
      $name,
      isset($config['title']) ? $config['title'] : dt('[Undefined]'),
      isset($config['description']) ? $config['description'] : '',
    );
  }

  drush_print_table($rows, TRUE);
  drush_log(dt('Found @count devgen config(s).', array(
    '@count' => count($configs),
  )), 'ok');
}


/**
 * Prints an output row.
 *
 * @param array $output
 *   An array of the output.
 * @param int $indent
 *   (optional = 0) The indent to apply to the current level.
 */
function _devgen_output_row(array $output, $indent = 0) {
  foreach ($output as $item) {
    if (is_array($item) && !isset($item['message'])) {
      _devgen_output_row($item, $indent + 2);
    }
    else {
      drush_print(dt('@indent@time seconds - !message', array(
        '@indent' => str_pad('', $indent, ' ', STR_PAD_LEFT),
        '@time' => $item['time'],
        '!message' => html_entity_decode($item['message']),
      )));
    }
  }
}

/**
 * Callback function for drush devgen-execute.
 */
function drush_devgen_execute($name = NULL) {
  if (empty($name)) {
    return drush_log(dt('Missing the "name" argument.'), 'error');
  }

  $config = devgen_config($name);
  if (empty($config)) {
    return drush_log(dt('Failed to find a devgen config with the name "@name".', array(
      '@name' => $name,
    )), 'error');
  }

  $devgen = devgen_new($name, $config);
  $evaluate = drush_get_option('eval', FALSE) !== FALSE;

  drush_log(dt('@action the devgen @name', array(
    '@action' => $evaluate ? t('Evaluating') : t('Executing'),
    '@name' => $devgen->getName(),
  )), 'ok');
  drush_print('');

  $devgen->execute($evaluate);
  _devgen_output_row($devgen->getOutput()->output(), 2);

  drush_print('');
  drush_log(dt('Completed @name', array(
    '@name' => $devgen->getName(),
  )), 'ok');
}

/**
 * Callback function for drush devgen-import.
 */
function drush_devgen_import($filename = NULL) {
  if (empty($filename)) {
    return drush_log(dt('Missing the "filename" argument.'), 'error');
  }

  $real_filename = realpath(dirname($filename)) . DIRECTORY_SEPARATOR . basename($filename);
  if (!file_exists($real_filename)) {
    return drush_log(dt('Unable to find the file @filename.', array(
      '@filename' => $real_filename,
    )), 'error');
  }

  $import = file_get_contents($real_filename);
  $name = drush_get_option('name');
  try {
    $config = devgen_import_validate($import, $name);
    if (devgen_save($config['name'], $config)) {
      drush_log(dt('Successfully imported config @name', array(
        '@name' => $config['name'],
      )), 'ok');
    }
    else {
      drush_log(dt('Failed to save import config @name', array(
        '@name' => $config['name'],
      )), 'warning');
    }
  }
  catch (Exception $err) {
    drush_log(dt('Failed to import to file: @filename, @message', array(
      '@filename' => $filename,
      '@message' => $err->getMessage(),
    )), 'warning');
  }
}

/**
 * Callback function for drush devgen-export.
 */
function drush_devgen_export($name = NULL) {
  if (empty($name)) {
    return drush_log(dt('Missing the "name" argument.'), 'error');
  }

  $config = devgen_config($name);
  if (empty($config)) {
    return drush_log(dt('Failed to find a devgen config with the name "@name".', array(
      '@name' => $name,
    )), 'error');
  }

  $export = devgen_new($name, $config)->__toString();
  $target = drush_get_option('target');
  if ($target) {
    $filename = realpath(dirname($target)) . DIRECTORY_SEPARATOR . basename($target);
    if (file_exists($filename)) {
      $write = in_array(
        drush_prompt(dt('The file currently exists, overwrite?'), 'y'),
        array('y', 'Y', 'yes', 'Yes')
      );
    }
    else {
      $write = TRUE;
    }

    if ($write) {
      try {
        file_put_contents($filename, "{$export}\n");

        drush_log(dt('Exported to file: @filename', array(
          '@filename' => $filename,
        )), 'ok');
      }
      catch (Exception $err) {
        drush_log(dt('Failed to export to file: @filename, @message', array(
          '@filename' => $filename,
          '@message' => $err->getMessage(),
        )), 'warning');
      }
    }
    else {
      drush_log(dt('Failed to export to file: @filename', array(
        '@filename' => $filename,
      )), 'warning');
    }
  }
  else {
    drush_print($export);
  }
}
