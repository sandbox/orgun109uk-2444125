<?php
/**
 * @file
 * Provides devgen tasks, conditions and properties.
 */

/**
 * Implements hook_devgens_alter().
 */
function devgen_devgens_alter(&$devgens) {
  $rows = db_query('SELECT * FROM {devgens}')
    ->fetchAllAssoc('name');

  foreach ($rows as $name => $devgen) {
    $storage = isset($devgens[$name]) ? 'overriden' : 'database';

    $devgens[$name] = (array) $devgen;
    if (is_string($devgens[$name]['properties'])) {
      $devgens[$name]['properties'] = @unserialize($devgens[$name]['properties']);
    }

    if (is_string($devgens[$name]['tasks'])) {
      $devgens[$name]['tasks'] = @unserialize($devgens[$name]['tasks']);
    }

    if (is_string($devgens[$name]['settings'])) {
      $devgens[$name]['settings'] = @unserialize($devgens[$name]['settings']);
    }

    $devgens[$name]['_storage'] = $storage;
  }
}

/**
 * Implements hook_devgen_tasks().
 */
function devgen_devgen_tasks() {
  return array(
    // DevGen actions.
    'drupalSetMessage' => array(
      'title' => t('Drupal set message'),
      'description' => t('Displays a message on-screen.'),
      'classname' => 'DevGenActionDrupalSetMessage',
    ),
    'watchdogMessage' => array(
      'title' => t('Watchdog message'),
      'description' => t('Adds a watchdog message.'),
      'classname' => 'DevGenActionWatchdogMessage',
    ),
    'createUser' => array(
      'title' => t('Create User'),
      'description' => t('Creates a new user entity.'),
      'classname' => 'DevGenActionCreateUser',
    ),
    'executeDevGen' => array(
      'title' => t('Execute DevGen'),
      'description' => t('Executes an additional devgen config.'),
      'classname' => 'DevGenActionDevGen',
    ),

    // DevGen conditions.
    'TrueFalseRandom' => array(
      'title' => t('Random (TRUE or FALSE)'),
      'description' => t('Randomly returns TRUE or FALSE.'),
      'classname' => 'DevGenConditionRandomTrueFalse',
    ),
    'verifyUser' => array(
      'title' => t('Verify User'),
      'description' => t('Verifies a user entity exists.'),
      'classname' => 'DevGenConditionVerifyUser',
    ),
  );
}

/**
 * Implements hook_devgen_properties().
 */
function devgen_devgen_properties() {
  return array(
    'string' => array(
      'title' => t('String'),
      'description' => t('A string value.'),
      'classname' => 'DevGenPropertyString',
    ),
    'randomString' => array(
      'title' => t('Random string'),
      'description' => t('Generates a random string.'),
      'classname' => 'DevGenPropertyRandomString',
    ),
    'number' => array(
      'title' => t('Number'),
      'description' => t('A numeric value.'),
      'classname' => 'DevGenPropertyNumber',
    ),
    'randomNumber' => array(
      'title' => t('Random number'),
      'description' => t('Generates a random number.'),
      'classname' => 'DevGenPropertyRandomNumber',
    ),
  );
}
