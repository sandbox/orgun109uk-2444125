<?php
/**
 * @file
 * Define a random number property value.
 */

/**
 * Class DevGenPropertyRandomNumber.
 */
class DevGenPropertyRandomNumber extends DevGenProperty {

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->value = NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function onValue() {
    if ($this->value === NULL) {
      $min = $this->getSetting('min', 0);
      $max = $this->getSetting('max', 0);

      $this->value = rand($min, $min == $max ? getrandmax() : $max);
    }

    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function settings() {
    $form = parent::settings();

    $form['min'] = array(
      '#type' => 'textfield',
      '#title' => t('Min value'),
      '#default_value' => $this->getSetting('min', 0),
    );

    $form['max'] = array(
      '#type' => 'textfield',
      '#title' => t('Max value'),
      '#default_value' => $this->getSetting('max', 0),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettings(array $settings, array &$errors) {
    if (!is_numeric($settings['min'])) {
      $errors['min'] = t('The min value must be numeric.');
      return FALSE;
    }

    if (!is_numeric($settings['max'])) {
      $errors['max'] = t('The max value must be numeric.');
      return FALSE;
    }

    if ($settings['min'] > $settings['max']) {
      $errors['min'] = t('The min value must be lower than the max.');
      return FALSE;
    }

    $max = getrandmax();
    if ($settings['max'] > $max) {
      $errors['max'] = t('The max value cannot exceed @max.', array(
        '@max' => $max,
      ));

      return FALSE;
    }

    return parent::validateSettings($settings, $errors);
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettings(array $settings) {
    parent::submitSettings($settings);

    if (count($settings)) {
      foreach ($settings as $setting => $value) {
        $this->setSetting($setting, $settings['min']);
      }
    }
  }

}
