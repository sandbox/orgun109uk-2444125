<?php
/**
 * @file
 * Define the string property value.
 */

/**
 * Class DevGenPropertyString.
 */
class DevGenPropertyString extends DevGenProperty {

  /**
   * {@inheritdoc}
   */
  public function settings() {
    $form = parent::settings();

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => $this->rawValue(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettings(array $settings) {
    parent::submitSettings($settings);

    $this->value = isset($settings['value']) ? $settings['value'] : NULL;
  }

}
