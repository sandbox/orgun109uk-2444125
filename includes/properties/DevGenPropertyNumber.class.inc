<?php
/**
 * @file
 * Define the number property value.
 */

/**
 * Class DevGenPropertyNumber.
 */
class DevGenPropertyNumber extends DevGenProperty {

  /**
   * {@inheritdoc}
   */
  public function settings() {
    $form = parent::settings();

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => $this->rawValue(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettings(array $settings, array &$errors) {
    if (!is_numeric($settings['value'])) {
      $errors['value'] = t('The value must be numeric.');
      return FALSE;
    }

    return parent::validateSettings($settings, $errors);
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettings(array $settings) {
    parent::submitSettings($settings);

    $this->value = isset($settings['value']) ? $settings['value'] : NULL;
  }

}
