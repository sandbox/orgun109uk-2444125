<?php
/**
 * @file
 * Define the random string property value.
 */

/**
 * Class DevGenPropertyRandomString.
 */
class DevGenPropertyRandomString extends DevGenProperty {

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->value = NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function onValue() {
    if ($this->value === NULL) {
      $characters = $this->getSetting('characters', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
      $length = $this->getSetting('length', 10);

      $chars = strlen($characters);
      $value = '';
      for ($i = 0; $i < $length; $i++) {
        $value .= $characters[rand(0, $chars - 1)];
      }

      $this->value = $this->getSetting('prefix', '') . $value . $this->getSetting('suffix', '');
    }

    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function settings() {
    $form = parent::settings();

    $form['characters'] = array(
      '#type' => 'textfield',
      '#title' => t('Allowed characters'),
      '#default_value' => $this->getSetting('characters', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),
    );

    $form['length'] = array(
      '#type' => 'textfield',
      '#title' => t('Length'),
      '#default_value' => $this->getSetting('length', '10'),
    );

    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => $this->getSetting('prefix', ''),
    );

    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Suffix'),
      '#default_value' => $this->getSetting('suffix', ''),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettings(array $settings, array &$errors) {
    if (!is_numeric($settings['length'])) {
      $errors['length'] = t('The length value must be numeric.');
      return FALSE;
    }

    if ($settings['length'] < 1) {
      $errors['length'] = t('The length value must be greater than 1.');
      return FALSE;
    }

    $max = getrandmax();
    if ($settings['length'] > $max) {
      $errors['length'] = t('The length value cannot exceed @max.', array(
        '@max' => $max,
      ));

      return FALSE;
    }

    return parent::validateSettings($settings, $errors);
  }

  /**
   * {@inheritdoc}
   */
  public function submitSettings(array $settings) {
    parent::submitSettings($settings);

    if (count($settings)) {
      foreach ($settings as $setting => $value) {
        $this->setSetting($setting, $value);
      }
    }
  }

}
