<?php
/**
 * @file
 * Provides the devgen action.
 */

/**
 * Create devgen action class.
 */
class DevGenActionDevGen extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $success = FALSE;
    $config = devgen_config($this->getSetting('config'));
    if ($config) {
      $devgen = devgen_new($config['name'], $config);
      $success = $devgen->execute($evaluate);

      $this->devgen->getOutput()->add($devgen->getOutput());
    }
    else {
      $this->devgen->message(t('Failed to find the devgen config @name.', array(
        '@name' => $this->getSetting('config'),
      )), 'error');
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $configs = devgen_devgen_configs();
    unset($configs[$this->devgen->getName()]);

    $form['config'] = array(
      '#type' => 'select',
      '#title' => t('DevGen config'),
      '#default_value' => $this->getSetting('config'),
      '#required' => TRUE,
    );

    return $form;
  }

}
