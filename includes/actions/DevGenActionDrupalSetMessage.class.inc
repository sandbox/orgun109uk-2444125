<?php
/**
 * @file
 * Provides the drupal-set-message action.
 */

/**
 * Create drupal-set-message class.
 */
class DevGenActionDrupalSetMessage extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    //@codingStandardsIgnoreStart
    $message = t($this->getSetting('message', NULL, TRUE));
    //@codingStandardsIgnoreEnd
    $type = $this->getSetting('type', 'info');

    if (!$evaluate) {
      drupal_set_message($message, $type);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['type'] = array(
      '#type' => 'selet',
      '#title' => t('Message type'),
      '#options' => array(
        'info' => t('Info'),
        'warning' => t('Warning'),
        'error' => t('Error'),
      ),
      '#default_value' => $this->getSetting('type'),
      '#required' => TRUE,
    );

    $form['message'] = array(
      '#type' => 'textfield',
      '#title' => t('Message'),
      '#default_value' => $this->getSetting('message'),
      '#required' => TRUE,
    );

    return $form;
  }

}
