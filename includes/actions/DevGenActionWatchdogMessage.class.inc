<?php
/**
 * @file
 * Provides the watchdog-message action.
 */

/**
 * Create watchdog-message class.
 */
class DevGenActionWatchdogMessage extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    if (!$evaluate) {
      watchdog(
        $this->getSetting('type', '', TRUE),
        $this->getSetting('message', '', TRUE),
        $this->getSetting('severity', WATCHDOG_NOTICE)
      );
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['severity'] = array(
      '#type' => 'selet',
      '#title' => t('Watchdog severity'),
      '#options' => array(
        WATCHDOG_EMERGENCY => t('Emergency'),
        WATCHDOG_ALERT => t('Alert'),
        WATCHDOG_CRITICAL => t('Critical'),
        WATCHDOG_ERROR => t('Error'),
        WATCHDOG_WARNING => t('Warning'),
        WATCHDOG_NOTICE => t('Notice'),
        WATCHDOG_INFO => t('Info'),
        WATCHDOG_DEBUG => t('Debug'),
      ),
      '#default_value' => $this->getSetting('type'),
      '#required' => TRUE,
    );

    $form['type'] = array(
      '#type' => 'textfield',
      '#title' => t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#required' => TRUE,
    );

    $form['message'] = array(
      '#type' => 'textfield',
      '#title' => t('Message'),
      '#default_value' => $this->getSetting('message'),
      '#required' => TRUE,
    );

    return $form;
  }

}
