<?php
/**
 * @file
 * Provides the create-user action.
 */

/**
 * Create user action class.
 */
class DevGenActionCreateUser extends DevGenAction {

  /**
   * {@inheritdoc}
   */
  protected function onResetProperties() {
    $properties = parent::onResetProperties();
    return array_merge($properties, array(
      'uid' => 'User ID',
      'name' => 'Username',
      'mail' => 'Email',
      'user' => 'User object',
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $name = $this->getSetting('name', NULL, TRUE);
    $mail = $this->getSetting('mail', NULL, TRUE);
    $password = $this->getSetting('password', NULL, TRUE);
    $roles = $this->getSetting('roles');
    if (empty($roles)) {
      $roles = array(DRUPAL_AUTHENTICATED_RID => 'authenticated user');
    }
    else {
      $current_roles = user_roles();
      $new_roles = array();

      foreach ($roles as $role) {
        if (in_array($role, $current_roles)) {
          $new_roles[array_search($role, $current_roles)] = $role;
        }
      }
    }

    $success = $evaluate ? TRUE : FALSE;

    $this->devgen->message('Creating user...');
    if (!$evaluate) {
      $fields = array(
        'name' => $name,
        'mail' => $mail,
        'pass' => empty($password) ? user_password(8) : $password,
        'status' => 1,
        'init' => $mail,
        'roles' => $new_roles,
      );

      $user = user_save('', $fields);
      $success = $user && isset($user->uid);
      if ($success) {
        $this->setProperty('uid', $user->uid);
        $this->setProperty('name', $user->name);
        $this->setProperty('mail', $user->mail);
        $this->setProperty('user', $user);
      }
    }
    else {
      $this->setProperty('uid', 0);
      $this->setProperty('name', $name);
      $this->setProperty('mail', $mail);
      $this->setProperty('user', (object) array(
        'uid' => 0,
        'name' => $name,
        'mail' => $mail,
      ));
    }

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->getSetting('name'),
      '#required' => TRUE,
    );

    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Mail'),
      '#default_value' => $this->getSetting('mail'),
      '#required' => TRUE,
    );

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $this->getSetting('password'),
      '#required' => TRUE,
    );

    $roles = user_roles();
    foreach ($roles as $rid => $role) {
      $role_options[$role] = $role;
    }

    $form['roles'] = array(
      '#type' => 'select',
      '#title' => t('Roles'),
      '#multiple' => TRUE,
      '#options' => $role_options,
      '#default_value' => $this->getSetting('roles'),
    );

    return $form;
  }

}
