<?php
/**
 * @file
 * Provides the verify-user condition.
 */

/**
 * Create verify-user condition class.
 */
class DevGenConditionVerifyUser extends DevGenCondition {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $field = $this->getSetting('field', 'name', TRUE);
    $operation = $this->getSetting('operation', '=');
    $value = $this->getSetting('value', NULL, TRUE);

    $condition = "{$field} {$operation} :value";
    $success = db_query_range("SELECT 1 FROM {users} WHERE {$condition}", 0, 1, array(
      ':value' => $operation == 'LIKE' ? ('%' . db_like($value) . '%') : $value,
    ))->fetchField() == 1;

    if ($this->getSetting('negate', FALSE)) {
      $success = !$success;
    }

    $this->devgen->message(t('Checking field "@field" is @op "@value"', array(
      '@field' => $field,
      '@op' => $operation,
      '@value' => $value,
    )));

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function settings(array $values = NULL) {
    $form = parent::settings($values);

    $form['field'] = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#description' => t('Select which field should be used to match the user.'),
      '#options' => array(
        'uid' => t('User ID'),
        'name' => t('Username'),
        'mail' => t('Email'),
      ),
      '#default_value' => $this->getSetting('field'),
      '#required' => TRUE,
    );

    $form['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#description' => t('The condition operation.'),
      '#options' => array(
        '=' => t('Equals'),
        '>=' => t('Greater or equals'),
        '>' => t('Greater'),
        '<=' => t('Lower or equals'),
        '<' => t('Lower'),
        'LIKE' => t('Like'),
      ),
      '#default_value' => $this->getSetting('operation'),
      '#required' => TRUE,
    );

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#description' => t('The condition value.'),
      '#default_value' => $this->getSetting('value'),
      '#required' => TRUE,
    );

    return $form;
  }

}
