<?php
/**
 * @file
 * Provides the TRUE-FALSE random condition.
 */

/**
 * Create a TRUE-FALSE random condition class.
 */
class DevGenConditionRandomTrueFalse extends DevGenCondition {

  /**
   * {@inheritdoc}
   */
  protected function onExecute($evaluate = FALSE) {
    $success = mt_rand(0, 1) == 1;
    if ($this->getSetting('negate', FALSE)) {
      $success = !$success;
    }

    return $success;
  }

}
