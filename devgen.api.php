<?php
/**
 * @file
 * The API file for devgen.
 */

/**
 * Register devgen configs.
 *
 * This should return an array of devgen configs indexed by the devgen
 * name.
 *
 * The information should contain the title, description, settings array, tasks
 * array and properties array.
 *
 * @return array
 *   An array of devgen configs.
 */
function hook_devgens() {
  return array(
    'devgen_example' => array(
      'title' => 'Example',
      'description' => 'An example DevGen configuration.',
      'settings' => array(
        'entry_point' => 'verify_user',
        'loops' => 10,
      ),
      'properties' => array(
        'username' => array(
          'name' => 'randomString',
          'value' => NULL,
          'config' => array(),
        ),
      ),
      'tasks' => array(
        'verify_user' => array(
          'name' => 'verifyUser',
          'config' => array(
            'negate' => 0,
            'field' => 'name',
            'operation' => '=',
            'value' => '@{username}',
            'success_exit_point' => 'skipped_user_message',
            'fail_exit_point' => 'create_user',
          ),
        ),
        'create_user' => array(
          'name' => 'createUser',
          'config' => array(
            'name' => '@{username}',
            'mail' => '@{username}@devgen.dev',
            'password' => 'password',
            'roles' => array(
              'authenticated user' => 'authenticated user',
            ),
            'success_exit_point' => 'created_user_message',
            'fail_exit_point' => '',
          ),
        ),
        'created_user_message' => array(
          'name' => 'drupalSetMessage',
          'config' => array(
            'message' => 'Successfully created the user @{create_user:name}',
            'success_exit_point' => '',
            'fail_exit_point' => '',
          ),
        ),
        'skipped_user_message' => array(
          'name' => 'drupalSetMessage',
          'config' => array(
            'message' => 'The user @{create_user:name} already exists.',
            'success_exit_point' => '',
            'fail_exit_point' => '',
          ),
        ),
      ),
    ),
  );
}

/**
 * Alter the registered devgen configs.
 *
 * @param array $configs
 *   A reference to the registered devgen configs array.
 */
function hook_devgens_alter(array &$configs) {
  $configs['devgen_example']['title'] = t('An alternative title');
}

/**
 * Register devgen tasks.
 *
 * This should return an array of task information arrays indexed by the task
 * name.
 *
 * The information should contain the title, description, the class name and if
 * required the file location (if you do not wish to define in the files
 * section of your modules info file).
 *
 * @return array
 *   An array of devgen tasks.
 */
function hook_devgen_tasks() {
  $module = drupal_get_path('module', 'devgen');
  return array(
    'createUser' => array(
      'title' => t('Create User'),
      'description' => t('Creates a new user entity.'),
      'classname' => 'DevGenTaskCreateUser',
      'file' => "{$module}/includes/tasks/DevGenTaskCreateUser.class.inc",
    ),
  );
}

/**
 * Alter the registered devgen tasks.
 *
 * @param array $tasks
 *   A reference to the registered tasks array.
 */
function hook_devgen_tasks_alter(array &$tasks) {
  $tasks['createUser']['title'] = t('An alternative title');
}

/**
 * Register devgen properties.
 *
 * This should return an array of property information arrays indexed by the
 * property name.
 *
 * The information should contain the title, description, the class name and if
 * required the file location (if you do not wish to define in the files
 * section of your modules info file).
 *
 * @return array
 *   An array of devgen properties.
 */
function hook_devgen_properties() {
  $module = drupal_get_path('module', 'devgen');
  return array(
    'string' => array(
      'title' => t('String'),
      'description' => t('A string value.'),
      'classname' => 'DevGenPropertyString',
      'file' => "{$module}/includes/properties/DevGenPropertyString.class.inc",
    ),
  );
}

/**
 * Alter the registered devgen properties.
 *
 * @param array $properties
 *   A reference to the registered properties array.
 */
function hook_devgen_properties_alter(array &$properties) {
  $properties['string']['title'] = t('An alternative title');
}
